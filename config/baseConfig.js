/* eslint-disable */
const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const isProduction =
  process.argv[process.argv.indexOf("--mode") + 1] === "production";

module.exports = {
  entry: {
    [process.env.npm_package_name]: isProduction
      ? [path.resolve("src", "main.js")]
      : [
          path.resolve("src", "main.js"),
          "webpack-hot-middleware/client?path=/__webpack_hmr&reload=true"
        ]
  },
  output: {
    path: path.resolve(process.env.GOPATH, "bin", "resources", "services"),
    filename: path.join(process.env.npm_package_name, "js", "bundle.js"),
    publicPath: isProduction
      ? "/sedmax/web/ui/"
      : "http://127.0.0.1:8080/sedmax/web/ui/",
    chunkFilename: path.join(
      process.env.npm_package_name,
      "js",
      "chunks",
      "[name].js"
    )
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "thread-loader",
            options: {
              workers: require('os').cpus().length - 1
            },
          }, "babel-loader"]
      },
      {
        test: /\.css/,
        use: [
          isProduction ? {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: path.resolve(process.env.GOPATH, "bin", "resources", "services"),
              hmr: process.env.NODE_ENV === !isProduction,
            },
          } : "style-loader",
          {
            loader: "thread-loader",
            options: {
              workers: require('os').cpus().length - 1,
              workerParallelJobs: 2,
              poolTimeout: 2000,
            },
          },
          "css-loader",
          "postcss-loader"
        ]
      },
      {
        test: /\.less$/,
        use: [
          isProduction ? {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: path.resolve(process.env.GOPATH, "bin", "resources", "services"),
              hmr: process.env.NODE_ENV === !isProduction,
            },
          } : "style-loader",
          {
            loader: "thread-loader",
            options: {
              workers: require('os').cpus().length - 1,
              workerParallelJobs: 2,
              poolTimeout: 2000,
            },
          },
          "css-loader",
          "postcss-loader",
          {
            loader: "less-loader",
            options: {
              paths: ['./node_modules'],
              modifyVars: {
                "text-color": "#000",
                "primary-color": "#326da8",
                // 'table-row-hover-bg': '#808080',
                "table-header-bg": "#eaeaea",
                "table-padding-vertical": "10px",
                "table-padding-horizontal": "14px",
                "font-family": "UbuntuLight",
                "body-background": "#dadfe1",
                "layout-body-background": "#fffff",
                "table-selected-row-bg": "#f5f5f5"
                // 'btn-danger-color': '#fff',
                // 'btn-danger-bg': '#ff4d4f',
                // 'btn-danger-border': '#ff4d4f'
                //
              },
              javascriptEnabled: true
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: [".js", ".jsx"],
    alias: {
      "@ant-design/icons/lib/dist$": path.resolve("./src/icons.js")
    }
  },
  plugins: [
    new webpack.DefinePlugin({
      __NAME__: JSON.stringify(process.env.npm_package_name),
      __VERSION__: JSON.stringify(process.env.npm_package_version)
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru|en/),
  ],
  node: {
    // prevent webpack from injecting mocks to Node native modules
    // that does not make sense for the client
    dgram: "empty",
    fs: "empty",
    net: "empty",
    tls: "empty",
    child_process: "empty"
  }
};
