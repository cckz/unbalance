/* eslint-disable */
const webpack = require('webpack');
const merge = require('webpack-merge');
const baseConfig = require('./baseConfig');

module.exports = merge(baseConfig, {
  mode: "development",
  // cheap-module-eval-source-map is faster for development
  devtool: 'cheap-module-eval-source-map',

  // devServer
  devServer: {
    hot: true,
    // contentBase: false, // since we use CopyWebpackPlugin.
    compress: true,
    host: '127.0.0.1',
    proxy: 'http://192.168.1.129',
    localPort: 8080,
    open: true,
    overlay: { warnings: false, errors: true },
    publicPath: '/sedmax/web/ui/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ]
});
