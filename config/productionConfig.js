/* eslint-disable */
const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const baseConfig = require('./baseConfig');

module.exports = merge(baseConfig, {
  mode: "production",
  devtool: 'hidden-source-map',
  optimization: {
    // splitChunks: {
    //   chunks: 'all',
    //   maxInitialRequests: Infinity,
    //   minSize: 0,
    //   cacheGroups: {
    //     recharts: {
    //       test: /[\\/]node_modules[\\/](recharts)[\\/]/,
    //       name: "recharts"
    //     },
    //     antd: {
    //       test: /[\\/]node_modules[\\/](antd|@ant-design)[\\/]/,
    //       name: "antd"
    //     },
    //   }
    // },
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: true,
        terserOptions: {
          compress: {
            warnings: false
          },
          output: {
            comments: false,
          },
        }
      }),
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: { safe: true, map: { inline: false } }
      })
    ]
  },
  plugins: [
    // enable scope hoisting
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.HashedModuleIdsPlugin(),
    new MiniCssExtractPlugin({
      filename: path.join(process.env.npm_package_name, "css", "bundle.css"),
      chunkFilename: path.join(process.env.npm_package_name, "css", "[id].css")
    }),
    new CopyWebpackPlugin(
      [
        {
          from: 'urls.json',
          to: path.join(process.env.npm_package_name, 'js')
        }
      ],
      { debug: 'info' }
    )
  ]
});
