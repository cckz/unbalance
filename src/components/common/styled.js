import styled from '@emotion/styled';
import { Button, Form, Icon } from 'antd';
import DebounceInput from 'react-debounce-input';

// Header
export const StyledSubHeader = styled.div`
  display: flex;
  min-height: 4rem;
  min-width: 80rem;
  flex-flow: row wrap;
  align-items: baseline;
  background-color: #808080;
  align-content: space-around;

  ::before {
    content: '';
    height: 4px;
    width: 100%;
    background: linear-gradient(#323232, #9a9a9a, #808080);
  }

  ::after {
    content: '';
    height: 3px;
    width: 100%;
    background: linear-gradient(#808080, #9a9a9a, #dadfe1);
  }
`;

export const StyledTitle = styled.h1`
  color: #fff;
  line-height: 2;
  font-size: 1.5rem;
  font-family: 'UbuntuBold';
  text-transform: uppercase;
  margin: 0.25rem 1rem 0.25rem 1rem;
`;

export const StyledActionSubHeader = styled.div`
  max-width: 60rem;
  display: flex;
  justify-content: flex-end;
  margin-inline-end: 0.7rem;
  margin-inline-start: auto;
  .ant-btn {
    margin-inline-start: 0.3rem;
    margin-inline-end: 0.3rem;
  }
`;

// Buttons
export const ButtonStyled = styled(Button)`
  font-size: 16px;
  margin-inline-start: 0.25rem;
  margin-inline-end: 0.25rem;
  margin-block-start: 0rem;
  margin-block-end: 0rem;
`;

export const ButtonBackStyled = styled(ButtonStyled)`
  margin-inline-start: 0.5rem;
`;

export const ButtonStyled18pxFontSize = styled(ButtonStyled)`
  font-size: 18px;
`;

// icon
export const IconStyled = styled(Icon)`
  font-size: 32px;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

// Content
export const ContentItem = styled.div`
  min-height: calc(100vh - 94px);
  background: #fff;
  padding: 1rem;
  margin-block-start: 0.3rem;
  margin-block-end: 0rem;
  margin-inline-start: 0.3rem;
  margin-inline-end: 0.3rem;
  border-radius: 4px !important;
  background-color: #fff;
`;

// Grid
export const FlexGrid = styled.div`
  display: flex;
  flex-direction: row;
  padding-block-end: 0.4rem;
`;

// Table title
export const TableTitle = styled.h5`
  text-transform: uppercase;
  font-weight: bold;
  margin-block-start: 1rem;
  margin-block-end: 1rem;
  margin-inline-end: auto;
`;

// Form
export const FormStyled = styled(Form)`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const StyledDebounceInput = styled(DebounceInput)`
  top: 0;
  z-index: 2;
  width: 100%;
  outline: none;
  font-size: 14px;
  position: sticky;
  padding: 6px 12px;
  background-color: #fff;
  border: 1px solid #e5e5e5;
  border-radius: 4px !important;
`;
