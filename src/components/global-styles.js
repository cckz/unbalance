import { css } from '@emotion/core';

export const styles = css`
  .above-line {
    display: none;
  }
  .ant-message {
    z-index: 9999 !important;
  }
  .ant-tag,
  .ant-btn,
  .ant-input,
  .ant-calendar-range,
  .ant-tooltip-inner,
  .ant-pagination-item,
  .ant-select-selection,
  .ant-pagination-item-link,
  .ant-popover-inner,
  .ant-select-dropdown,
  .ant-select-dropdown-menu,
  .ant-calendar-time-picker-btn,
  .ant-table-filter-dropdown,
  .ant-message-notice-content {
    border-radius: 4px !important;
  }
  .ant-switch,
  .ant-click-animating-node {
    border-radius: 100px !important;
  }
  .ant-input {
    font-size: 14px !important;
    line-height: 1.5 !important;
  }
  .ant-tree-checkbox-inner {
    border-radius: 2px !important;
  }
  .ant-radio-inner {
    border-radius: 100px !important;
  }
  .ant-form-explain {
    text-align: center;
  }
  .ant-checkbox-inner {
    border-radius: 2px !important;
  }
  .pop-up-events {
    bottom: 40px;
  }
  .ant-calendar-picker-container {
    .ant-calendar-footer {
      line-height: 32px;
    }
    .ant-calendar-footer-btn {
      display: flex;
      align-items: center;
      .ant-calendar-range-quick-selector {
        flex: 5;
      }
      .ant-calendar-time-picker-btn {
        flex: 2;
        color: #fff;
        background-color: #326da8;
        border-color: #326da8;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
        box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
        text-align: center;
        transition: all 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
        :hover {
          color: #fff;
          background-color: #5387b5;
          border-color: #5387b5;
        }
      }
      .ant-calendar-ok-btn {
        display: none;
      }
      .ant-calendar-time-picker-btn-disabled {
        background: #d9d9d9 !important;
        :hover {
          color: rgba(0, 0, 0, 0.25) !important;
        }
      }
      .ant-tag.ant-tag-blue {
        padding-inline-start: 0.3rem;
        padding-inline-end: 0.3rem;
      }
    }
  }
`;
