import React, { lazy, Suspense } from 'react';
import { Global } from '@emotion/core';
import { Route, BrowserRouter as Router } from 'react-router-dom';

import { styles } from './global-styles';
import ErrorBoundary from './error-boundary';
import { routers } from '../constants/routers';
import ReportsList from './reports-list/reports-list';

const StatementsList = lazy(() =>
  import(
    /* webpackChunkName: "StatementsList", webpackPrefetch: true */ './statements-list/statements-list'
  )
);

const ReportEditor = lazy(() =>
  import(
    /* webpackChunkName: "ReportEditor", webpackPrefetch: true */ './report-editor/report-editor'
  )
);

const App = () => (
  <>
    <Global styles={styles} />
    <Router basename="sedmax/web/ui/unbalance_reports/">
      <ErrorBoundary>
        <Route
          exact
          path={routers.reports_list}
          render={props => <ReportsList {...props} />}
        />
        <Route
          path={`${routers.info}/:id`}
          render={props => (
            <Suspense fallback={null}>
              <StatementsList {...props} />
            </Suspense>
          )}
        />
        <Route
          path={[routers.create_template, `${routers.view}/:id`]}
          render={props => (
            <Suspense fallback={null}>
              <ReportEditor {...props} />
            </Suspense>
          )}
        />
      </ErrorBoundary>
    </Router>
  </>
);

export default App;
