import React, { Component } from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import { Alert, Divider } from 'antd';

const AlertStyled = styled(Alert)`
  margin: 1rem;
`;

const Info = styled.p`
  font-style: italic;
  margin-left: 4.5rem;
  white-space: pre-wrap;
`;

const ErrorMessage = styled.p`
  color: #f5212d;
  font-weight: bolder;
  margin-top: -0.42rem;
`;

const Summary = styled.summary`
  outline: none;
  display: block;
  cursor: pointer;
  font-size: 16px;
  margin: 1rem - 0.2rem;

  &:before {
    content: '📖';
    padding-right: 0.4rem;
  }

  &::-webkit-details-marker {
    display: none;
  }
`;

export default class ErrorBoundary extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]).isRequired
  };

  state = { error: null, info: null };

  /**
   * Clearing saved error states into local storage at callback,
   * so app won't crush after reload of the page.
   */
  componentDidCatch(error, info) {
    /* eslint-disable-next-line no-underscore-dangle, no-undef */
    this.setState({ error, info }, () => localStorage.clear());
  }

  render() {
    const { children } = this.props;

    const { error, info } = this.state;

    if (error) {
      return (
        <AlertStyled
          showIcon
          type="error"
          iconType="setting"
          message={
            <ErrorMessage>
              Что-то пошло не так. Пожалуйста, перезагрузите страницу.
              Обратитесь к администратору системы, если ошибка возникает
              постоянно
            </ErrorMessage>
          }
          description={
            <details>
              <Summary>Прочитать техническую информацию</Summary>
              <Divider orientation="left">
                <span role="img" aria-label="error-type">
                  🛠️ Error description :
                </span>
              </Divider>
              <Info>{error.toString()}</Info>
              <Divider orientation="left">
                <span role="img" aria-label="error-stack">
                  🛠️ Component stack :
                </span>
              </Divider>
              <Info>{info.componentStack.trimLeft()}</Info>
            </details>
          }
        />
      );
    }

    return children;
  }
}
