import React, { useMemo } from 'react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { func, shape } from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { Button, Tooltip } from 'antd';

import { BaseTable } from '../../base-table/base-table';

import { routers } from '../../../constants/routers';
import { tablesIDs } from '../../../constants/tables-ids';
import { getClientHeight } from '../../../client-settings/selectors';
import { getStatusFetchingReports, getReports } from '../redux/selectors';
import { changeVisibilityCopyReportModal } from '../modal-copy-report/redux/actions';

const StyledButton = styled(Button)`
  margin-inline-start: 0.3rem;
  margin-inline-end: 0.3rem;
`;

const ReportsTable = ({ intl: { formatMessage } }) => {
  const dispatch = useDispatch();
  const reports = useSelector(getReports);
  const clientHeight = useSelector(getClientHeight);
  const isFetching = useSelector(getStatusFetchingReports);

  const getColumnWidth = columName => {
    if (columName === 'id') return 80;
    if (columName === 'actions') return 110;
    if (columName === 'period') return 300;
    return false;
  };

  const getSorter = (a, b, columName) => {
    if (columName === 'id') return a.id - b.id;
    const first = a[columName].toLowerCase();
    const second = b[columName].toLowerCase();
    if (first < second) return -1;
    if (first > second) return 1;
    return 0;
  };

  const columns = useMemo(
    () =>
      ['id', 'name', 'period', 'actions'].map(columName => ({
        title: formatMessage({ id: `labels.${columName}` }),
        dataIndex: columName,
        key: columName,
        align: columName === 'actions' ? 'center' : 'left',
        width: getColumnWidth(columName),
        className: columName === 'actions' && 'table__cell',
        sorter:
          columName !== 'actions'
            ? (a, b) => getSorter(a, b, columName)
            : false,
        render(column, record) {
          if (columName === 'actions') {
            return (
              <>
                <Tooltip
                  placement="top"
                  title={formatMessage({ id: 'reportsList.btns.edit' })}
                >
                  <Link to={`${routers.view}/${record.id}`}>
                    <StyledButton icon="edit" />
                  </Link>
                </Tooltip>
                <Tooltip
                  placement="top"
                  title={formatMessage({ id: 'reportsList.btns.copy' })}
                >
                  <StyledButton
                    icon="copy"
                    onClick={e => {
                      e.currentTarget.blur();
                      dispatch(changeVisibilityCopyReportModal(record.id));
                    }}
                  />
                </Tooltip>
              </>
            );
          }
          if (columName === 'period') {
            return column === '1d'
              ? formatMessage({ id: 'reportsList.day' })
              : formatMessage({ id: 'reportsList.month' });
          }
          if (columName === 'name') {
            return <Link to={`${routers.info}/${record.id}`}>{column}</Link>;
          }
          return column;
        }
      })),
    [clientHeight]
  );

  return (
    <BaseTable
      tableID={tablesIDs.reportsList}
      clientHeight={clientHeight}
      isFetching={isFetching}
      dataSource={reports}
      columns={columns}
    />
  );
};

ReportsTable.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(ReportsTable);
