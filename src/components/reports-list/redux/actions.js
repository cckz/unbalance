import {
  COPY_REPORT_FAILURE,
  COPY_REPORT_REQUEST,
  COPY_REPORT_SUCCESS,
  REPORTS_LIST_REQUEST,
  REPORTS_LIST_FAILURE,
  REPORTS_LIST_SUCCESS,
  DELETE_SELECTED_REPORTS_FAILURE,
  DELETE_SELECTED_REPORTS_REQUEST,
  DELETE_SELECTED_REPORTS_SUCCESS
} from './constants';

export const reportsListRequest = () => ({
  type: REPORTS_LIST_REQUEST
});

export const reportsListSuccess = reports => ({
  type: REPORTS_LIST_SUCCESS,
  reports
});

export const reportsListFailure = error => ({
  type: REPORTS_LIST_FAILURE,
  error
});

export const deleteSelectedReportsRequest = () => ({
  type: DELETE_SELECTED_REPORTS_REQUEST
});

export const deleteSelectedReportsSuccess = () => ({
  type: DELETE_SELECTED_REPORTS_SUCCESS
});

export const deleteSelectedReportsFailure = error => ({
  type: DELETE_SELECTED_REPORTS_FAILURE,
  error
});

export const copyReportRequest = ({ id, name }) => ({
  type: COPY_REPORT_REQUEST,
  id,
  name
});

export const copyReportSuccess = () => ({
  type: COPY_REPORT_SUCCESS
});

export const copyReportFailure = error => ({
  type: COPY_REPORT_FAILURE,
  error
});
