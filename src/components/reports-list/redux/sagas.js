import { call, put, takeLatest, select, race, take } from 'redux-saga/effects';

import {
  CONFIRM_NO,
  CONFIRM_YES,
  showConfirmation
} from '../../../messages/actions';
import { API_PREFIX, fetchJSON } from '../../../redux/api';
import { intl } from '../../intl-global-provider/intl-global-provider';
import { errorMessage, successMessage } from '../../../messages/message';

import {
  copyReportFailure,
  copyReportSuccess,
  reportsListRequest,
  reportsListSuccess,
  reportsListFailure,
  deleteSelectedReportsFailure,
  deleteSelectedReportsSuccess
} from './actions';

import { changeVisibilityCopyReportModal } from '../modal-copy-report/redux/actions';

import {
  COPY_REPORT_REQUEST,
  REPORTS_LIST_REQUEST,
  DELETE_SELECTED_REPORTS_REQUEST
} from './constants';
import { tablesIDs } from '../../../constants/tables-ids';

export function* requestAllReports() {
  try {
    const data = yield call(fetchJSON, `${API_PREFIX}/all`);
    yield put(reportsListSuccess(data));
  } catch (error) {
    errorMessage(
      error.response.data
        ? error.response.data.message
        : `Status: ${error.response.status}`
    );
    yield put(
      reportsListFailure({
        message: error.response.data ? error.response.data.message : null,
        status: error.response.status
      })
    );
  }
}
// костыль для тестов
// FIXME tablesIDs.reportsList захардкожен
export const getCheckedRows = state => state.checkedRows[tablesIDs.reportsList];

export function* requestDeleteSelectedReports() {
  const idsSeelctedReports = yield select(getCheckedRows);
  if (idsSeelctedReports && idsSeelctedReports.length) {
    try {
      yield put(
        showConfirmation({
          title: 'messages.confirm',
          message: ''
        })
      );
      const { yes } = yield race({
        yes: take(CONFIRM_YES),
        no: take(CONFIRM_NO)
      });
      if (yes) {
        yield call(fetchJSON, `${API_PREFIX}/remove`, {
          method: 'POST',
          data: JSON.stringify(idsSeelctedReports)
        });
        // для информации
        yield put(deleteSelectedReportsSuccess());
        successMessage(
          intl.formatMessage({
            id: `${
              idsSeelctedReports.length > 1
                ? 'toastr.deleteSuccess'
                : 'toastr.singleDeleteSuccess'
            }`
          })
        );
        yield put(reportsListRequest());
      }
    } catch (error) {
      console.log(error);
      errorMessage(
        error.response.data
          ? error.response.data.message
          : `Status: ${error.response.status}`
      );
      yield put(
        deleteSelectedReportsFailure({
          message: error.response.data ? error.response.data.message : null,
          status: error.response.status
        })
      );
    }
  } else {
    errorMessage(intl.formatMessage({ id: 'toastr.noSelectedError' }));
  }
}

export function* requestCopyReport({ id, name }) {
  try {
    const idNewReport = yield call(fetchJSON, `${API_PREFIX}/copy/${id}`, {
      method: 'POST',
      data: JSON.stringify(name)
    });
    // для информации об успешном копировании
    yield put(copyReportSuccess());
    yield put(changeVisibilityCopyReportModal());
    if (idNewReport.id) {
      yield put(reportsListRequest());
      successMessage(intl.formatMessage({ id: 'toastr.copySuccess' }));
    }
  } catch (error) {
    errorMessage(
      error.response.data
        ? error.response.data.message
        : `Status: ${error.response.status}`
    );
    yield put(
      copyReportFailure({
        message: error.response.data ? error.response.data.message : null,
        status: error.response.status
      })
    );
  }
}

export function* watchReports() {
  yield takeLatest(COPY_REPORT_REQUEST, requestCopyReport);
  yield takeLatest(REPORTS_LIST_REQUEST, requestAllReports);
  yield takeLatest(
    DELETE_SELECTED_REPORTS_REQUEST,
    requestDeleteSelectedReports
  );
}
