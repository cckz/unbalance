import { combineReducers } from 'redux';
import {
  REPORTS_LIST_REQUEST,
  REPORTS_LIST_FAILURE,
  REPORTS_LIST_SUCCESS
} from './constants';

import {
  INITIAL_REPORTS_SUCCESS,
  INITIAL_REPORTS_REQUEST,
  INITIAL_REPORTS_FAILURE
} from '../initialization/actions';

export const list = (state = [], action) => {
  switch (action.type) {
    case REPORTS_LIST_SUCCESS:
    case INITIAL_REPORTS_SUCCESS:
      return action.reports;
    default:
      return state;
  }
};

export const isFetching = (state = false, action) => {
  switch (action.type) {
    case REPORTS_LIST_REQUEST:
    case INITIAL_REPORTS_REQUEST:
      return true;
    case REPORTS_LIST_SUCCESS:
    case REPORTS_LIST_FAILURE:
    case INITIAL_REPORTS_FAILURE:
    case INITIAL_REPORTS_SUCCESS:
      return false;
    default:
      return state;
  }
};

export const reports = combineReducers({
  list,
  isFetching
});
