import { call, select } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';

import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import rootSaga from '../../../../redux/sagas';
import { API_PREFIX, fetchJSON } from '../../../../redux/api';
import { fetchPostFailure } from '../../../../../tests/__mock__/api';

import {
  copyReportFailure,
  copyReportRequest,
  copyReportSuccess,
  reportsListFailure,
  reportsListRequest,
  reportsListSuccess,
  deleteSelectedReportsSuccess,
  deleteSelectedReportsFailure,
  deleteSelectedReportsRequest
} from '../actions';
import { reportsList } from '../../../../../tests/__mock__/reports.mock';
import { getCheckedRows } from '../sagas';
import { confirmYes, showConfirmation } from '../../../../messages/actions';

// import { confirmYes, showConfirmation } from '../../../../messages/actions';

import {
  error404,
  errorResponse
} from '../../../../../tests/__mock__/errors.mock';
import { changeVisibilityCopyReportModal } from '../../modal-copy-report/redux/actions';

describe('reports-list sagas', () => {
  it('Запрашиваем все отчеты: succeess', () => {
    const url = `${API_PREFIX}/all`;

    return expectSaga(rootSaga)
      .provide([[call(fetchJSON, url), reportsList]])
      .put(reportsListSuccess(reportsList))
      .dispatch(reportsListRequest())
      .silentRun();
  });

  it('Запрашиваем все отчеты: failure', () => {
    const url = `${API_PREFIX}/all`;
    return expectSaga(rootSaga)
      .provide([[call(fetchJSON, url), throwError(errorResponse)]])
      .put(reportsListFailure(error404))
      .dispatch(reportsListRequest())
      .silentRun();
  });

  it('Копирование отчета: succeess', () => {
    const [firstReprot] = reportsList;
    const url = `${API_PREFIX}/copy/${firstReprot.id}`;

    return (
      expectSaga(rootSaga)
        .provide([
          [
            call(fetchJSON, url, {
              method: 'POST',
              data: JSON.stringify('New name report')
            }),
            {}
          ]
        ])
        .put(copyReportSuccess())
        // .put(reportsListRequest())
        .put(changeVisibilityCopyReportModal())
        .dispatch(
          copyReportRequest({ id: firstReprot.id, name: 'New name report' })
        )
        .silentRun()
    );
  });
  it('Копирование отчета: failure', () => {
    const [firstReprot] = reportsList;
    const url = `${API_PREFIX}/copy/${firstReprot.id}`;

    return expectSaga(rootSaga)
      .provide([
        [
          call(fetchJSON, url, {
            method: 'POST',
            data: JSON.stringify('New name report')
          }),
          throwError(errorResponse)
        ]
      ])
      .put(copyReportFailure(error404))
      .dispatch(
        copyReportRequest({ id: firstReprot.id, name: 'New name report' })
      )
      .silentRun();
  });

  // it('Удаление отчетов: succeess', () => {
  //   const url = `${API_PREFIX}/remove`;
  //
  //   return expectSaga(rootSaga)
  //     .provide([
  //       [select(getCheckedRows), [1, 2, 3]],
  //       [
  //         call(fetchJSON, url, {
  //           method: 'POST',
  //           data: JSON.stringify([1, 2, 3])
  //         }),
  //         {}
  //       ]
  //     ])
  //
  //     .put(deleteSelectedReportsSuccess())
  //     .put(
  //       showConfirmation({
  //         title: 'messages.confirm',
  //         message: ''
  //       })
  //     )
  //     .dispatch(deleteSelectedReportsRequest())
  //     .dispatch(confirmYes())
  //     .silentRun();
  // });

  // it('Удаление отчетов: failure', () => {
  //   const url = `${API_PREFIX}/remove`;
  //
  //   return expectSaga(rootSaga)
  //     .provide([
  //       [select(getCheckedRows), [1, 2, 3]],
  //       [
  //         call(fetchJSON, url, {
  //           method: 'POST',
  //           data: JSON.stringify([1, 2, 3])
  //         }),
  //         throwError(errorResponse)
  //       ]
  //     ])
  //
  //     .put(deleteSelectedReportsFailure(error404))
  //     .put(
  //       showConfirmation({
  //         title: 'messages.confirm',
  //         message: ''
  //       })
  //     )
  //     .dispatch(deleteSelectedReportsRequest())
  //     .dispatch(confirmYes())
  //     .silentRun();
  // });
});
