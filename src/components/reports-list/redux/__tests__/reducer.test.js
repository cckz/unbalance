import {
  isFetching as isFetchingReportsReducer,
  list as reportsListReducer
} from '../reducers';
import {
  reportsListSuccess,
  reportsListFailure,
  reportsListRequest
} from '../actions';

import {
  initialReportsSuccess,
  initialReportsRequest,
  initialReportsFailure
} from '../../initialization/actions';

import { error404 } from '../../../../../tests/__mock__/errors.mock';
import { reportsList } from '../../../../../tests/__mock__/reports.mock';

describe('reports-list reducers', () => {
  // Reports List
  it('initial state списка отчетов', () => {
    expect(reportsListReducer(undefined, {})).toEqual([]);
  });
  it('Запрос всех отчетов: success ', () => {
    expect(reportsListReducer([], reportsListSuccess(reportsList))).toEqual(
      reportsList
    );
  });
  it('Запрос данных при инициализации: success ', () => {
    expect(
      reportsListReducer(
        [],
        initialReportsSuccess({ reports: reportsList, clientHeight: 700 })
      )
    ).toEqual(reportsList);
  });

  // isFetching
  it('initialState состояния isFetching', () => {
    expect(isFetchingReportsReducer(undefined, {})).toEqual(false);
  });
  it('статус isFetching = true', () => {
    expect(isFetchingReportsReducer(false, reportsListRequest())).toEqual(true);
    expect(
      isFetchingReportsReducer(
        false,
        initialReportsRequest({ clientHeight: 700 })
      )
    ).toEqual(true);
  });
  it('статус isFetching = false', () => {
    expect(
      isFetchingReportsReducer(true, reportsListSuccess(reportsList))
    ).toEqual(false);
    expect(
      isFetchingReportsReducer(true, reportsListFailure(error404))
    ).toEqual(false);

    expect(
      isFetchingReportsReducer(
        true,
        initialReportsSuccess({ reports: reportsList, clientHeight: 700 })
      )
    ).toEqual(false);
    expect(
      isFetchingReportsReducer(true, initialReportsFailure(error404))
    ).toEqual(false);
  });
});
