import configureStore from 'redux-mock-store';

import {
  reportsListFailure,
  reportsListRequest,
  reportsListSuccess,
  copyReportFailure,
  copyReportRequest,
  copyReportSuccess,
  deleteSelectedReportsFailure,
  deleteSelectedReportsRequest,
  deleteSelectedReportsSuccess
} from '../actions';

import {
  REPORTS_LIST_SUCCESS,
  REPORTS_LIST_FAILURE,
  REPORTS_LIST_REQUEST,
  COPY_REPORT_FAILURE,
  COPY_REPORT_REQUEST,
  COPY_REPORT_SUCCESS,
  DELETE_SELECTED_REPORTS_FAILURE,
  DELETE_SELECTED_REPORTS_REQUEST,
  DELETE_SELECTED_REPORTS_SUCCESS
} from '../constants';

import { error404 } from '../../../../../tests/__mock__/errors.mock';
import { reportsList } from '../../../../../tests/__mock__/reports.mock';

const mockStore = configureStore();
const store = mockStore();

describe('reports-list actions', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('Запрос всех отчетов: request', () => {
    const expectedActions = [
      {
        type: REPORTS_LIST_REQUEST
      }
    ];
    store.dispatch(reportsListRequest());
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('Запрос всех шаблонов: success', () => {
    const expectedActions = [
      {
        type: REPORTS_LIST_SUCCESS,
        reports: reportsList
      }
    ];
    store.dispatch(reportsListSuccess(reportsList));
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('handles sets request failure', () => {
    const expectedActions = [
      {
        type: REPORTS_LIST_FAILURE,
        error: error404
      }
    ];
    store.dispatch(reportsListFailure(error404));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('Запрос копирование отчета: request', () => {
    const expectedActions = [
      {
        type: COPY_REPORT_REQUEST,
        id: 1,
        name: 'Новый отчет'
      }
    ];
    store.dispatch(copyReportRequest({ id: 1, name: 'Новый отчет' }));
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('Запрос копирование отчета: success', () => {
    const expectedActions = [
      {
        type: COPY_REPORT_SUCCESS
      }
    ];
    store.dispatch(copyReportSuccess());
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('Запрос копирование отчета: failure', () => {
    const expectedActions = [
      {
        type: COPY_REPORT_FAILURE,
        error: error404
      }
    ];
    store.dispatch(copyReportFailure(error404));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('Запрос удаление отчетов: request', () => {
    const expectedActions = [
      {
        type: DELETE_SELECTED_REPORTS_REQUEST
      }
    ];
    store.dispatch(deleteSelectedReportsRequest());
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('Запрос удаление отчетов:: success', () => {
    const expectedActions = [
      {
        type: DELETE_SELECTED_REPORTS_SUCCESS
      }
    ];
    store.dispatch(deleteSelectedReportsSuccess());
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('Запрос удаление отчетов: failure', () => {
    const expectedActions = [
      {
        type: DELETE_SELECTED_REPORTS_FAILURE,
        error: error404
      }
    ];
    store.dispatch(deleteSelectedReportsFailure(error404));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
