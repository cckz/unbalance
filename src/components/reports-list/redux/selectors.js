export const getReports = state => state.reports.list;
export const getStatusFetchingReports = state => state.reports.isFetching;
