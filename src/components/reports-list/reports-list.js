import React, { useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { func, shape } from 'prop-types';
import { injectIntl } from 'react-intl';
import { Helmet } from 'react-helmet';

import SubHeader from './subheader/subheader';
import { ContentItem } from '../common/styled';
import ReportsTable from './reports-table/reports-table';

import { offset } from '../../constants/offset';
import { initialReportsRequest } from './initialization/actions';
import ModalCopyWrappedForm from './modal-copy-report/modal-copy-report';
import { changeClientContentSize } from '../../client-settings/action';

const ReportsList = ({ intl: { formatMessage } }) => {
  const contentRef = useRef(null);
  const dispatch = useDispatch();
  useEffect(() => {
    if (contentRef.current && contentRef.current.clientHeight > 0) {
      dispatch(
        initialReportsRequest({
          clientHeight: contentRef.current.clientHeight - offset
        })
      );
    }
    const changeSizes = () =>
      dispatch(
        changeClientContentSize(contentRef.current.clientHeight - offset)
      );
    window.addEventListener('resize', changeSizes);
    return () => window.removeEventListener('resize', changeSizes);
  }, [contentRef, window.innerHeight]);

  return (
    <>
      <Helmet title={formatMessage({ id: 'reportsList.titleWithSedmax' })} />
      <SubHeader />
      <ContentItem ref={contentRef}>
        <ReportsTable />
      </ContentItem>
      <ModalCopyWrappedForm />
    </>
  );
};

ReportsList.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(ReportsList);
