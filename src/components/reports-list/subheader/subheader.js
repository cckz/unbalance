import React from 'react';
import { Link } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { shape, func } from 'prop-types';
import { useDispatch } from 'react-redux';

import { routers } from '../../../constants/routers';
import { deleteSelectedReportsRequest } from '../redux/actions';

import {
  StyledTitle,
  StyledSubHeader,
  ButtonStyled,
  StyledActionSubHeader
} from '../../common/styled';

const SubHeaderReportsList = ({ intl: { formatMessage } }) => {
  const dispatch = useDispatch();
  return (
    <StyledSubHeader>
      <StyledTitle>{formatMessage({ id: 'reportsList.title' })}</StyledTitle>
      <StyledActionSubHeader>
        <ButtonStyled
          icon="delete"
          type="danger"
          data-testid="delete_report_button"
          onClick={() => dispatch(deleteSelectedReportsRequest())}
        >
          {formatMessage({ id: 'reportsList.btns.delete' })}
        </ButtonStyled>
        <Link to={routers.create_template}>
          <ButtonStyled
            icon="plus"
            type="primary"
            data-testid="add_report_button"
          >
            {formatMessage({ id: 'reportsList.btns.add' })}
          </ButtonStyled>
        </Link>
      </StyledActionSubHeader>
    </StyledSubHeader>
  );
};

SubHeaderReportsList.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(SubHeaderReportsList);
