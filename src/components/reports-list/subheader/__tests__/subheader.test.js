import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import SubHeaderReportsList from '../subheader';
import {
  renderWithIntlAndRedux,
  cleanup,
  intl
} from '../../../../../tests/setupTests';

describe('SubHeaderReportsList', () => {
  afterEach(cleanup);
  it('subheader render', () => {
    // Arrange
    const { getByText, container, getByTestId } = renderWithIntlAndRedux(
      <Router>
        <SubHeaderReportsList intl={intl} />
      </Router>
    );

    const title = getByText(intl.formatMessage({ id: 'reportsList.title' }))
      .textContent;
    const deleteButton = getByTestId('delete_report_button');
    const addButton = getByTestId('add_report_button');
    // Act

    // Assert
    expect(deleteButton.textContent).toBe(
      intl.formatMessage({ id: 'reportsList.btns.delete' })
    );
    expect(addButton.textContent).toBe(
      intl.formatMessage({ id: 'reportsList.btns.add' })
    );
    expect(title).toBe(intl.formatMessage({ id: 'reportsList.title' }));
    expect(container).toMatchSnapshot();
  });
});
