export const INITIAL_REPORTS_REQUEST = 'INITIAL_REPORTS_REQUEST';
export const INITIAL_REPORTS_SUCCESS = 'INITIAL_REPORTS_SUCCESS';
export const INITIAL_REPORTS_FAILURE = 'INITIAL_REPORTS_FAILURE';

export const initialReportsRequest = ({ clientHeight }) => ({
  type: INITIAL_REPORTS_REQUEST,
  clientHeight
});

export const initialReportsSuccess = ({ reports, clientHeight }) => ({
  type: INITIAL_REPORTS_SUCCESS,
  reports,
  clientHeight
});

export const initialReportsFailure = error => ({
  type: INITIAL_REPORTS_FAILURE,
  error
});
