import { call, put } from 'redux-saga/effects';

import { initialReportsFailure, initialReportsSuccess } from './actions';
import { API_PREFIX, fetchJSON } from '../../../redux/api';
import { errorMessage } from '../../../messages/message';

export function* requestInitiaReportsList({ clientHeight }) {
  try {
    const data = yield call(fetchJSON, `${API_PREFIX}/all`);

    yield put(
      initialReportsSuccess({
        reports: data,
        clientHeight
      })
    );
  } catch (error) {
    errorMessage(
      error.response.data
        ? error.response.data.message
        : `Status: ${error.response.status}`
    );
    yield put(
      initialReportsFailure({
        message: error.response.data ? error.response.data.message : null,
        status: error.response.status
      })
    );
  }
}
