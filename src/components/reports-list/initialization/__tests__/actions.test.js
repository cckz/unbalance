import configureStore from 'redux-mock-store';

import {
  initialReportsFailure,
  initialReportsRequest,
  initialReportsSuccess,
  INITIAL_REPORTS_FAILURE,
  INITIAL_REPORTS_REQUEST,
  INITIAL_REPORTS_SUCCESS
} from '../actions';

import { error404 } from '../../../../../tests/__mock__/errors.mock';
import { reportsList } from '../../../../../tests/__mock__/reports.mock';

const mockStore = configureStore();
const store = mockStore();

describe('initial reports-list', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('report-list request action', () => {
    const expectedActions = [
      {
        type: INITIAL_REPORTS_REQUEST,
        clientHeight: 700
      }
    ];
    store.dispatch(
      initialReportsRequest({
        clientHeight: 700
      })
    );
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('report-list success action', () => {
    const expectedActions = [
      {
        type: INITIAL_REPORTS_SUCCESS,
        reports: reportsList,
        clientHeight: 700
      }
    ];
    store.dispatch(
      initialReportsSuccess({
        reports: reportsList,
        clientHeight: 700
      })
    );
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('report-list failure action', () => {
    const expectedActions = [
      {
        type: INITIAL_REPORTS_FAILURE,
        error: error404
      }
    ];
    store.dispatch(initialReportsFailure(error404));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
