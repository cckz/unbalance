import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';

import rootSaga from '../../../../redux/sagas';
import { API_PREFIX, fetchJSON } from '../../../../redux/api';

import {
  initialReportsSuccess,
  initialReportsRequest,
  initialReportsFailure
} from '../actions';
import { reportsList } from '../../../../../tests/__mock__/reports.mock';

import {
  error404,
  errorResponse
} from '../../../../../tests/__mock__/errors.mock';

describe('reports-list initialization sagas', () => {
  it('Запрашиваем данные для инициализации: succeess', () => {
    const url = `${API_PREFIX}/all`;

    return expectSaga(rootSaga)
      .provide([[call(fetchJSON, url), reportsList]])
      .put(
        initialReportsSuccess({
          reports: reportsList,
          clientHeight: 700
        })
      )
      .dispatch(initialReportsRequest({ clientHeight: 700 }))
      .silentRun();
  });

  it('Запрашиваем данные для инициализации: failure', () => {
    const url = `${API_PREFIX}/all`;
    return expectSaga(rootSaga)
      .provide([[call(fetchJSON, url), throwError(errorResponse)]])
      .put(initialReportsFailure(error404))
      .dispatch(initialReportsRequest({ clientHeight: 700 }))
      .silentRun();
  });
});
