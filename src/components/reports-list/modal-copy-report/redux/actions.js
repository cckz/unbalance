export const COPY_REPORT_MODAL_VISIBILITY = 'COPY_REPORT_MODAL_VISIBILITY';

export const changeVisibilityCopyReportModal = id => ({
  type: COPY_REPORT_MODAL_VISIBILITY,
  id
});
