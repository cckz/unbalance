import { COPY_REPORT_MODAL_VISIBILITY } from './actions';

export const initState = {
  visible: false,
  id: null
};

export const copyReportModal = (state = initState, action) => {
  if (action.type === COPY_REPORT_MODAL_VISIBILITY)
    return { visible: !state.visible, id: state.id ? null : action.id };
  return state;
};
