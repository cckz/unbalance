import {
  copyReportModal as copyReportModalReducer,
  initState
} from '../reducer';
import { changeVisibilityCopyReportModal } from '../actions';

describe('modal-copy-report reducers', () => {
  it('initial state', () => {
    expect(copyReportModalReducer(undefined, {})).toEqual(initState);
  });
  // return { visible: !state.visible, id: state.id ? null : action.id };
  it('Смена состояние модалки, открытие', () => {
    expect(
      copyReportModalReducer(initState, changeVisibilityCopyReportModal(1))
    ).toEqual({
      visible: true,
      id: 1
    });
  });

  it('Смена состояние модалки, закртыие ', () => {
    expect(
      copyReportModalReducer(
        {
          visible: true,
          id: 1
        },
        changeVisibilityCopyReportModal()
      )
    ).toEqual(initState);
  });
});
