import configureStore from 'redux-mock-store';

import {
  COPY_REPORT_MODAL_VISIBILITY,
  changeVisibilityCopyReportModal
} from '../actions';

const mockStore = configureStore();
const store = mockStore();

describe('modal-copy-report actions', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('Состояние модалки с id копируемого отчета', () => {
    const expectedActions = [
      {
        type: COPY_REPORT_MODAL_VISIBILITY,
        id: 1
      }
    ];
    store.dispatch(changeVisibilityCopyReportModal(1));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
