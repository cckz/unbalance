import React from 'react';
import ModalCopyWrappedForm, { ModalCopyReport } from '../modal-copy-report';
import {
  renderWithIntlAndRedux,
  cleanup,
  intl
} from '../../../../../tests/setupTests';

describe('ModalCopyReport', () => {
  afterEach(cleanup);
  it('modal-copy-report render', () => {
    // Arrange
    const { getByText, container } = renderWithIntlAndRedux(
      <ModalCopyWrappedForm intl={intl} />,
      {
        initialState: {
          modalsState: {
            copyReportModal: { visible: true, id: 1 }
          }
        }
      }
    );

    const title = getByText(intl.formatMessage({ id: 'messages.copyReport' }))
      .textContent;
    // const footer = container.querySelector('.ant-modal-footer')[0];
    // Act

    // Assert
    // expect(footer.children.length).toBe(2);
    expect(title).toBe(intl.formatMessage({ id: 'messages.copyReport' }));
    expect(container).toMatchSnapshot();
  });
});
