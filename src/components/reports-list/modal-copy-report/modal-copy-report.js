import React from 'react';
import styled from '@emotion/styled';
import { injectIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { func, shape, objectOf, any } from 'prop-types';
import { Form, Icon, Input, Modal as AntModal } from 'antd';

import { ButtonStyled } from '../../common/styled';

import { copyReportRequest } from '../redux/actions';
import { formsIDS } from '../../../constants/formsIDS';
import { errorMessage } from '../../../messages/message';
import { getStateModalCopyReport } from './redux/selecteros';
import { changeVisibilityCopyReportModal } from './redux/actions';

const StyledFormItem = styled(Form.Item)`
  width: 100%;
  .ant-col.ant-form-item-control-wrapper {
    width: 100%;
  }
`;

export const ModalCopyReport = ({ intl: { formatMessage }, form }) => {
  const dispatch = useDispatch();
  const { visible, id } = useSelector(getStateModalCopyReport);
  const { getFieldDecorator, validateFields } = form;

  const validateTypeField = (rule, value, callback) => {
    if (value) return callback();
    const errorMsg = `${formatMessage({
      id: 'messages.nameReportRequiredField'
    })}`;
    errorMessage(errorMsg);
    return callback(errorMsg);
  };

  const handleSubmit = e => {
    e.preventDefault();
    validateFields((error, { name }) => {
      if (error) {
        errorMessage(
          `${formatMessage({
            id: 'messages.nameReportRequiredField'
          })}`
        );
      } else {
        dispatch(copyReportRequest({ id, name }));
      }
    });
  };

  return (
    <AntModal
      title={formatMessage({ id: 'messages.copyReport' })}
      visible={visible}
      closable={false}
      width={400}
      destroyOnClose
      onCancel={() => dispatch(changeVisibilityCopyReportModal())}
      // getContainer={() => document.getElementsByClassName('page-container')[0]}
      footer={[
        <ButtonStyled
          key="cancel"
          onClick={() => dispatch(changeVisibilityCopyReportModal())}
        >
          {formatMessage({ id: 'btns.cancel' })}
        </ButtonStyled>,
        <ButtonStyled
          key="submit"
          type="primary"
          htmlType="submit"
          form={formsIDS.COPY_REPORT}
        >
          {formatMessage({ id: 'btns.save' })}
        </ButtonStyled>
      ]}
    >
      <Form layout="inline" onSubmit={handleSubmit} id={formsIDS.COPY_REPORT}>
        <StyledFormItem help="">
          {getFieldDecorator('name', {
            rules: [
              {
                validator: validateTypeField
              }
            ]
          })(
            <Input
              placeholder={formatMessage({ id: 'messages.copyRequest' })}
              prefix={<Icon type="copy" style={{ color: 'rgba(0,0,0,.25)' }} />}
            />
          )}
        </StyledFormItem>
      </Form>
    </AntModal>
  );
};

ModalCopyReport.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired,
  form: objectOf(any).isRequired
};

const ModalCopyWrappedForm = Form.create({ name: formsIDS.COPY_REPORT })(
  ModalCopyReport
);

export default injectIntl(ModalCopyWrappedForm);
