import React, { useEffect, useState } from 'react';
import { Icon, Table } from 'antd';
import styled from '@emotion/styled';
import { useDispatch, useSelector } from 'react-redux';
import { bool, any, number, string, arrayOf, func } from 'prop-types';

import { changeCheckedRows } from './redux/actions';

const TableStyled = styled(Table)`
  .ant-table-header,
  .ant-table-body {
    ::-webkit-scrollbar-track {
      border-left: 0px !important;
    }
    ::-webkit-scrollbar {
      width: 6px;
    }
  }

  table {
    font-size: 14px !important;
  }

  .ant-table-header::-webkit-scrollbar-track {
    border-top: 1px solid #b9b8b8;
    border-right: 1px solid #b9b8b8;
    border-bottom: 1px solid #b9b8b8;
  }

  thead[class*='ant-table-thead'] {
    th {
      font-weight: bold;
      border: 1px solid #b9b8b8 !important;
    }
    th:last-of-type {
      ${({ pagination: { pageSize, pageSizeOptions } }) =>
        pageSize === +pageSizeOptions[0]
          ? 'border-right: 1px solid #b9b8b8;'
          : 'border-right: none !important;'}
    }
  }

  .table__cell {
    ${({ tableID }) =>
      tableID !== 'statementsList' &&
      `padding-block-start: 0.3rem !important;
       padding-block-end: 0.3rem !important;`}
    ${({ pagination: { pageSize, pageSizeOptions } }) =>
      pageSize > +pageSizeOptions[0] && 'border-right: none !important;'}
  }

  ${({ isCheckableTable }) =>
    isCheckableTable &&
    `
    .ant-table-selection-column {
      width: 3% !important;
    }
    .ant-table-header col:first-of-type,
    .ant-table-body col:first-of-type {
      width: 3% !important;
    }
    `};

  .custom__pagination {
    display: flex;
    width: 100%;
    margin-block-start: 10px !important;
    li:last-of-type {
      order: -1;
      margin-inline-end: auto;
    }
  }

  .ant-table-footer {
    border: none !important;
    background: #fff;
    text-align: right;
    padding-block-end: 0;
  }

  .ant-table-pagination {
    margin: 6px 0 0 !important;
  }
`;

export const BaseTable = ({
  footer,
  columns,
  tableID,
  isFetching,
  dataSource,
  rowKey = 'id',
  clientHeight,
  isCheckableTable = true
}) => {
  const dispatch = useDispatch();
  const selectedRowKeys =
    useSelector(state => state.checkedRows[tableID]) || [];

  const [pageSize, setPageSize] = useState(13);
  const pageSizeRange = [`${Math.trunc(clientHeight / 38)}`, '30', '60', '100'];

  useEffect(() => {
    setPageSize(Math.trunc(clientHeight / 38));
  }, [clientHeight]);

  const rowSelection = {
    selectedRowKeys,
    manualSelect: (event, id) => {
      const currentTag = event.target.tagName.toLowerCase();
      if (currentTag !== 'td') return;
      if (selectedRowKeys.includes(id)) {
        dispatch(
          changeCheckedRows({
            tableID,
            ids: selectedRowKeys.filter(key => key !== id)
          })
        );
      } else {
        dispatch(changeCheckedRows({ tableID, ids: [...selectedRowKeys, id] }));
      }
    },
    onSelect: (record, selected) => {
      dispatch(
        changeCheckedRows({
          tableID,
          ids: selected
            ? [...selectedRowKeys, record.id]
            : selectedRowKeys.filter(id => id !== record.id)
        })
      );
    },
    onSelectAll: (selected, selectedRows) => {
      dispatch(
        changeCheckedRows({
          tableID,
          ids: selected ? selectedRows.map(({ id }) => id) : []
        })
      );
    }
  };

  return (
    <TableStyled
      tableID={tableID}
      isCheckableTable={isCheckableTable}
      scroll={{ y: pageSize > pageSizeRange[0] ? clientHeight : false }}
      bordered
      rowKey={rowKey}
      size="middle"
      columns={columns}
      dataSource={dataSource}
      rowSelection={isCheckableTable ? rowSelection : null}
      footer={footer}
      onRow={record => ({
        onClick: event =>
          isCheckableTable
            ? rowSelection.manualSelect(event, record[rowKey])
            : {}
      })}
      pagination={{
        className: 'custom__pagination',
        pageSize,
        position: 'bottom',
        pageSizeOptions: pageSizeRange,
        size: 'small',
        showSizeChanger: true,
        hideOnSinglePage: tableID === 'statementsList',
        total: !isFetching && dataSource.length,
        showTotal: (total, range) => `${range[0]}-${range[1]} из ${total}`,
        onShowSizeChange: (current, size) => setPageSize(size)
      }}
      loading={{
        delay: 100,
        spinning: isFetching,
        style: { paddingTop: '28.125%' },
        indicator: <Icon type="loading" style={{ fontSize: 32 }} />
      }}
      sortDirections={['descend', 'ascend']}
    />
  );
};

BaseTable.propTypes = {
  footer: func,
  isFetching: bool.isRequired,
  isCheckableTable: bool,
  columns: arrayOf(any),
  dataSource: arrayOf(any),
  rowKey: string,
  clientHeight: number.isRequired,
  tableID: string.isRequired
};

BaseTable.defaultProps = {
  isCheckableTable: true,
  rowKey: 'id',
  footer: undefined,
  columns: [],
  dataSource: []
};
