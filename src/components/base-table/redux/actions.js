import { CHECKED_ROWS } from './constants';

export const changeCheckedRows = ({ tableID, ids }) => ({
  type: CHECKED_ROWS,
  ids,
  tableID
});
