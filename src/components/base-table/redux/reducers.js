import { CHECKED_ROWS } from './constants';

import { INITIAL_REPORTS_REQUEST } from '../../reports-list/initialization/actions';
import { INITIAL_REPORT_EDITOR_REQUEST } from '../../report-editor/initialization/actions';
import { ADD_SELECTED_CHANNELS } from '../../report-editor/modal-settings-report/table-available-channels/redux/actions';
import { DELETE_CHANNELS_FROM_ADDED_TABLE } from '../../report-editor/modal-settings-report/table-added-channels/redux/actions';

export const checkedRows = (state = {}, action) => {
  switch (action.type) {
    case CHECKED_ROWS:
      return { ...state, [action.tableID]: action.ids };
    case ADD_SELECTED_CHANNELS:
    case INITIAL_REPORTS_REQUEST:
    case INITIAL_REPORT_EDITOR_REQUEST:
    case DELETE_CHANNELS_FROM_ADDED_TABLE:
      return {};
    default:
      return state;
  }
};
