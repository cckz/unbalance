import configureStore from 'redux-mock-store';

import { CHECKED_ROWS } from '../constants';
import { changeCheckedRows } from '../actions';
import { tablesIDs } from '../../../../constants/tables-ids';

const mockStore = configureStore();
const store = mockStore();

describe('base-table actions', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('Экшен на выделение строк в таблицк страницы reportsList', () => {
    const expectedActions = [
      {
        type: CHECKED_ROWS,
        ids: [1, 2, 3, 4],
        tableID: tablesIDs.reportsList
      }
    ];
    store.dispatch(
      changeCheckedRows({ tableID: 'reportsList', ids: [1, 2, 3, 4] })
    );
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('Экшен на выделение строк в таблицк страницы availableChannels', () => {
    const expectedActions = [
      {
        type: CHECKED_ROWS,
        ids: [1, 2, 3, 4],
        tableID: tablesIDs.availableChannels
      }
    ];
    store.dispatch(
      changeCheckedRows({ tableID: 'availableChannels', ids: [1, 2, 3, 4] })
    );
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('Экшен на выделение строк в таблицк страницы addedChannels', () => {
    const expectedActions = [
      {
        type: CHECKED_ROWS,
        ids: [1, 2, 3, 4],
        tableID: tablesIDs.addedChannels
      }
    ];
    store.dispatch(
      changeCheckedRows({ tableID: 'addedChannels', ids: [1, 2, 3, 4] })
    );
    expect(store.getActions()).toEqual(expectedActions);
  });
});
