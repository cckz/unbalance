import configureStore from 'redux-mock-store';

import { checkedRows as checkedRowsReducer } from '../reducers';

import { CHECKED_ROWS } from '../constants';
import { INITIAL_REPORTS_REQUEST } from '../../../reports-list/initialization/actions';
import { INITIAL_REPORT_EDITOR_REQUEST } from '../../../report-editor/initialization/actions';
import { tablesIDs } from '../../../../constants/tables-ids';
import { ADD_SELECTED_CHANNELS } from '../../../report-editor/modal-settings-report/table-available-channels/redux/actions';
import { DELETE_CHANNELS_FROM_ADDED_TABLE } from '../../../report-editor/modal-settings-report/table-added-channels/redux/actions';

const mockStore = configureStore();
const store = mockStore();

describe('base-table reducers', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('initialState checkedRows', () => {
    expect(checkedRowsReducer(undefined, {})).toEqual({});
  });
  it('Проверяем выбор строк в таблице', () => {
    expect(
      checkedRowsReducer(0, {
        type: CHECKED_ROWS,
        tableID: tablesIDs.reportsList,
        ids: [1]
      })
    ).toEqual({
      [tablesIDs.reportsList]: [1]
    });
    expect(
      checkedRowsReducer(0, {
        type: CHECKED_ROWS,
        tableID: tablesIDs.availableChannels,
        ids: [2]
      })
    ).toEqual({
      [tablesIDs.availableChannels]: [2]
    });
    expect(
      checkedRowsReducer(0, {
        type: CHECKED_ROWS,
        tableID: tablesIDs.addedChannels,
        ids: [3]
      })
    ).toEqual({
      [tablesIDs.addedChannels]: [3]
    });
  });
  it('Проверяем сброс состояния', () => {
    expect(
      checkedRowsReducer(0, {
        type: INITIAL_REPORTS_REQUEST,
        ids: [1, 2]
      })
    ).toEqual({});
    expect(
      checkedRowsReducer(0, {
        type: INITIAL_REPORT_EDITOR_REQUEST,
        ids: [1, 2, 3]
      })
    ).toEqual({});
    expect(
      checkedRowsReducer(0, {
        type: ADD_SELECTED_CHANNELS,
        checkedIDs: [1, 2]
      })
    ).toEqual({});
    expect(
      checkedRowsReducer(0, {
        type: DELETE_CHANNELS_FROM_ADDED_TABLE,
        ids: [1, 2, 3]
      })
    ).toEqual({});
  });
});
