import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { BaseTable } from '../base-table';

import { reportsList } from '../../../../tests/__mock__/reports.mock';
import {
  intl,
  cleanup,
  fireEvent,
  renderWithIntlAndRedux
} from '../../../../tests/setupTests';
import { initState } from '../../../../tests/__mock__/init-state.mock';

const getColumnWidth = columName => {
  if (columName === 'id') return 80;
  if (columName === 'actions') return 110;
  if (columName === 'period') return 300;
  return false;
};

const columns = ['id', 'name', 'period', 'actions'].map(columName => ({
  title: intl.formatMessage({ id: `labels.${columName}` }),
  dataIndex: columName,
  key: columName,
  align: columName === 'actions' ? 'center' : 'left',
  width: getColumnWidth(columName)
}));

describe('base-table component', () => {
  afterEach(cleanup);
  it('render a base-table', () => {
    // Arrange
    // const createSet = jest.fn();
    // const changeStateModalCreateSet = jest.fn();

    const { container, getByText, rerenderWithRedux } = renderWithIntlAndRedux(
      <Router>
        <BaseTable columns={columns} dataSource={reportsList} />
      </Router>
    );

    expect(container).toMatchSnapshot();

    // Act
    const [firstReport] = reportsList;
    const checkbox = container.querySelector(
      `[data-row-key="${firstReport.id}"] input[type=checkbox]`
    );
    expect(checkbox.checked).toBe(false);

    const leftClick = { button: 0 };
    fireEvent.click(getByText(firstReport.name), leftClick);
    expect(checkbox.checked).toBe(true);

    // const submitButton = getByText('OK');
    // const cancelButton = getByText('Cancel');
    // submitButton.click();
    // cancelButton.click();

    // Assert
    // expect(createSet).toHaveBeenCalledTimes(1);
    // expect(changeStateModalCreateSet).toHaveBeenCalledTimes(1);
    // rerenderWithRedux(
    //   <Router>
    //     <BaseTable />
    //   </Router>,
    //   nextState
    // );
    expect(container).toMatchSnapshot();
  });
});
