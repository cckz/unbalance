import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { any, func, objectOf, shape } from 'prop-types';
import { injectIntl } from 'react-intl';
import { Helmet } from 'react-helmet';

import Excel from './excel/excel';
import SubHeader from './subheader/subheader';
import { ContentItem } from '../common/styled';
import ModalSettingsWrappedForm from './modal-settings-report/modal-settings-report';

import { getReport } from './redux/selectors';
import { routers } from '../../constants/routers';
import { changeClientContentSize } from '../../client-settings/action';
import { initialReportEditorRequest } from './initialization/actions';

const ReportEditor = ({ intl: { formatMessage }, history, match }) => {
  const contentRef = useRef(null);
  const { idNewReport } = useSelector(getReport);
  const dispatch = useDispatch();
  useEffect(() => {
    if (contentRef.current && contentRef.current.clientHeight > 0) {
      dispatch(
        initialReportEditorRequest({
          clientHeight: contentRef.current.clientHeight,
          id: match.params.id
        })
      );
    }
    const changeSizes = () =>
      dispatch(changeClientContentSize(contentRef.current.clientHeight));
    window.addEventListener('resize', changeSizes);
    return () => window.removeEventListener('resize', changeSizes);
  }, [contentRef, window.innerHeight, match.params.id]);

  useEffect(() => {
    if (idNewReport) {
      history.push(`${routers.view}/${idNewReport}`);
    }
  }, [idNewReport]);

  return (
    <>
      <Helmet
        title={formatMessage({ id: 'reportEditor.titlesWithSedmax.create' })}
      />
      <SubHeader id={match.params.id} />
      <ContentItem ref={contentRef}>
        <Excel id={match.params.id} />
      </ContentItem>
      <ModalSettingsWrappedForm />
    </>
  );
};

ReportEditor.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired,
  history: shape({
    push: func.isRequired
  }).isRequired,
  match: objectOf(any).isRequired
};

export default injectIntl(ReportEditor);
