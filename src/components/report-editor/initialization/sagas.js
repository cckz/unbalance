import { call, put, all } from 'redux-saga/effects';

import {
  initialReportEditorFailure,
  initialReportEditorSuccess
} from './actions';
import { API_PREFIX, fetchJSON } from '../../../redux/api';
import { errorMessage } from '../../../messages/message';

export const generateIDsForChannels = channels =>
  channels.map(channel =>
    channel.billing_group_id === 0
      ? { ...channel, id: `${channel.device_id}_${channel.code}` }
      : { ...channel, id: `${channel.device_id}_${channel.billing_group_id}` }
  );

export function* requestInitialReportEditor({ clientHeight, id }) {
  try {
    const [readReport, channels, tree] = yield all([
      id
        ? yield call(fetchJSON, `${API_PREFIX}/read/${id}`)
        : yield call(fetchJSON, `${API_PREFIX}/template`),
      yield call(fetchJSON, `${API_PREFIX}/channels`),
      yield call(
        fetchJSON,
        `/sedmax/configuration/tree/${JSON.stringify({
          electro_archive: {
            group: 'indications',
            add_parameters: true,
            add_billing_groups: true
          }
        })}`
      )
    ]);

    // это говно без id, в старом модуле вообшще куча говна
    const channelssUpdate = generateIDsForChannels(channels);

    if (readReport.report) {
      readReport.report.channels = generateIDsForChannels(
        readReport.report.channels
      );
    }

    yield put(
      initialReportEditorSuccess({
        tree,
        clientHeight,
        channels: channelssUpdate,
        report: readReport.report || {},
        blocked_rows: readReport.blocked_rows || {},
        sheet: id ? readReport.sheet : readReport
      })
    );
  } catch (error) {
    errorMessage(
      error.response.data
        ? error.response.data.message
        : `Status: ${error.response.status}`
    );
    yield put(
      initialReportEditorFailure({
        message: error.response.data ? error.response.data.message : null,
        status: error.response.status
      })
    );
  }
}
