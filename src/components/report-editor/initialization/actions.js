export const INITIAL_REPORT_EDITOR_REQUEST = 'INITIAL_REPORT_EDITOR_REQUEST';
export const INITIAL_REPORT_EDITOR_SUCCESS = 'INITIAL_REPORT_EDITOR_SUCCESS';
export const INITIAL_REPORT_EDITOR_FAILURE = 'INITIAL_REPORT_EDITOR_FAILURE';

export const initialReportEditorRequest = ({ clientHeight, id }) => ({
  type: INITIAL_REPORT_EDITOR_REQUEST,
  clientHeight,
  id
});

export const initialReportEditorSuccess = ({
  sheet,
  tree,
  channels,
  clientHeight,
  report,
  blocked_rows
}) => ({
  type: INITIAL_REPORT_EDITOR_SUCCESS,
  tree,
  channels,
  sheet,
  clientHeight,
  report,
  blocked_rows
});

export const initialReportEditorFailure = error => ({
  type: INITIAL_REPORT_EDITOR_FAILURE,
  error
});
