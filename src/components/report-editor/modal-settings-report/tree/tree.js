import React, { useMemo, useState } from 'react';
import styled from '@emotion/styled';
import { injectIntl } from 'react-intl';
import { func, shape } from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import { Icon, Tree, Collapse, Skeleton, Spin } from 'antd';

import { TableTitle, StyledDebounceInput } from '../../../common/styled';

import { changeCheckedNodes } from './redux/actions';
import { getTreeStructure, getFlatStructure } from './redux/selectors';
import { getStatusFetchingReportEditor } from '../../redux/selectors';

const { DirectoryTree, TreeNode } = Tree;
const { Panel } = Collapse;

const DirectoryTreeStyled = styled(DirectoryTree)`
  li {
    padding: 0 !important;
    :not(:last-of-type):before {
      top: -20px !important;
    }
    :last-child:before {
      height: 13px;
      content: ' ';
      margin: 0;
      width: 10px;
      top: 0;
      left: 12px;
      position: absolute;
      border-left: 1px solid #d9d9d9;
    }
    span.ant-tree-switcher-noop {
      background: transparent !important;
      > i {
        visibility: hidden;
      }
      :after {
        content: ' ';
        margin: 0;
        width: 10px;
        height: 1px;
        top: 50%;
        left: 12px;
        position: absolute;
        border-bottom: 1px solid #d9d9d9;
      }
    }
    .ant-tree-node-content-wrapper {
      height: 100%;
      width: 90%;
    }
  }
`;

const CollapseStyled = styled(Collapse)`
  .ant-collapse-header {
    padding-inline-start: 18px !important;
    padding-block-start: 0.5rem !important;
    padding-block-end: 0.5rem !important;
    .anticon {
      left: 0 !important;
    }
  }
`;

export const IconStyled = styled(Icon)`
  font-size: 32px;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

const StyledDebounceInputCustom = styled(StyledDebounceInput)`
  margin-block-end: 1rem;
`;

const TreeDevices = ({ intl: { formatMessage } }) => {
  const [{ searchValue, expandedKeys }, setLocalTreeState] = useState({
    searchValue: '',
    expandedKeys: []
  });

  const dispatch = useDispatch();
  const flatStructure = useSelector(getFlatStructure);
  const treeStructure = useSelector(getTreeStructure);
  const isFetching = useSelector(getStatusFetchingReportEditor);

  const handleSearch = ({ target: { value } }) => {
    if (!value) {
      setLocalTreeState({ searchValue: '', expandedKeys: [] });
    } else {
      const findNode = ({ parent }) =>
        flatStructure.find(({ id }) => parent === id);

      const searchKeys = new Set();
      flatStructure.forEach(node => {
        const isMatching =
          node.text.toLowerCase().indexOf(value.toLowerCase()) > -1;

        const shouldFindParentKeys =
          isMatching && !searchKeys.has(node.parent) && node.parent !== '#';

        if (shouldFindParentKeys) {
          searchKeys.add(node.parent);
          let lastNode = node;
          while (lastNode.parent !== '#') {
            lastNode = findNode(lastNode);
            if (!searchKeys.has(lastNode.parent) && lastNode.parent !== '#') {
              searchKeys.add(lastNode.parent);
            } else break;
          }
        }
      });
      setLocalTreeState({ searchValue: value, expandedKeys: [...searchKeys] });
    }
  };

  const renderTreeWithSearch = (structure, highlightText) =>
    structure.map(({ key, title, children }) => {
      const index = title.toLowerCase().indexOf(highlightText.toLowerCase());
      const soughtString = title.substr(index, highlightText.length);
      const suffix = title.substr(index + highlightText.length);
      const prefix = title.substr(0, index);
      const newTitle =
        index > -1 ? (
          <span>
            {prefix}
            <span style={{ color: '#f50' }}>{soughtString}</span>
            {suffix}
          </span>
        ) : (
          <span>{title}</span>
        );

      if (children) {
        return (
          <TreeNode
            isLeaf={false}
            key={key}
            title={newTitle}
            icon={({ expanded }) => (
              <Icon
                type={expanded ? 'folder-open' : 'folder'}
                theme="twoTone"
              />
            )}
          >
            {renderTreeWithSearch(children, highlightText)}
          </TreeNode>
        );
      }

      return (
        <TreeNode
          isLeaf
          key={key}
          title={newTitle}
          icon={<Icon type="mobile" theme="twoTone" />}
        />
      );
    });

  const tree = useMemo(() => renderTreeWithSearch(treeStructure, searchValue), [
    treeStructure,
    searchValue
  ]);

  const handleCheckedNodes = checkedKyes => {
    const filterSelectedNodes = checkedKyes
      .filter(node => !node.indexOf('dev') || !node.indexOf('bg'))
      .map(nodes => (!nodes.indexOf('dev') ? nodes.slice(4) : nodes));
    dispatch(changeCheckedNodes(filterSelectedNodes));
  };

  return (
    <CollapseStyled bordered={false} defaultActiveKey={['title_tree']}>
      <Panel
        header={
          <TableTitle>
            {formatMessage({ id: 'optionsSelector.title' })}
          </TableTitle>
        }
        key="title_tree"
      >
        <Spin
          spinning={isFetching}
          indicator={<Icon type="loading" style={{ fontSize: 32 }} />}
        >
          <Skeleton
            active
            title={false}
            loading={isFetching}
            paragraph={{ rows: 3 }}
          >
            <StyledDebounceInputCustom
              minLength={0}
              debounceTimeout={800}
              onChange={handleSearch}
              placeholder={formatMessage({
                id: 'optionsSelector.enterTheName'
              })}
            />
            <DirectoryTreeStyled
              showLine
              showIcon
              checkable
              selectable={false}
              onExpand={keys =>
                setLocalTreeState({ searchValue, expandedKeys: keys })
              }
              onCheck={checkedKyes => {
                handleCheckedNodes(checkedKyes);
              }}
              expandedKeys={expandedKeys}
            >
              {tree}
            </DirectoryTreeStyled>
          </Skeleton>
        </Spin>
      </Panel>
    </CollapseStyled>
  );
};

TreeDevices.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(TreeDevices);
