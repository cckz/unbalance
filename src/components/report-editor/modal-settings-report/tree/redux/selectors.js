import { createSelector } from 'reselect';

export const getCheckedNodes = state =>
  state.reportEditor.treeDevices.checkedNodes;

export const getFlatStructure = state =>
  state.reportEditor.treeDevices.flatStructure;

const convertFlatToTree = (structure, root = '#') => {
  return structure.reduce((currentLevelNodes, { id, text, entity, parent }) => {
    if (parent === root) {
      const node = {
        entity,
        key: id,
        title: text
      };

      if (structure.some(s => s.parent === id)) {
        return [
          ...currentLevelNodes,
          {
            ...node,
            children: convertFlatToTree(structure, id)
          }
        ];
      }

      return [...currentLevelNodes, node];
    }

    return currentLevelNodes;
  }, []);
};

export const getTreeStructure = createSelector(
  getFlatStructure,
  structure => {
    return convertFlatToTree(structure);
  }
);
