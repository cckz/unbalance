export const CHANGE_CHECKED_NODES = 'CHANGE_CHECKED_NODES';

export const changeCheckedNodes = selectedNodes => ({
  type: CHANGE_CHECKED_NODES,
  selectedNodes
});
