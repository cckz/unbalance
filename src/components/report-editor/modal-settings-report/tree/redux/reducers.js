import { combineReducers } from 'redux';
import { CHANGE_CHECKED_NODES } from './actions';
import { INITIAL_REPORT_EDITOR_SUCCESS } from '../../../initialization/actions';

const flatStructure = (state = [], action) => {
  if (action.type === INITIAL_REPORT_EDITOR_SUCCESS) return action.tree;
  return state;
};

const checkedNodes = (state = [], action) => {
  if (action.type === CHANGE_CHECKED_NODES) return action.selectedNodes;
  return state;
};

export const treeDevices = combineReducers({
  checkedNodes,
  flatStructure
});
