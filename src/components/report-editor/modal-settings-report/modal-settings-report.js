import React, { useEffect } from 'react';
import { injectIntl } from 'react-intl';
import { Modal as AntModal } from 'antd';
import { func, shape } from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import TreeDevices from './tree/tree';
import SettingsForm from './settings-form/settings-form';
import { ButtonStyled, FlexGrid } from '../../common/styled';
import SelectedChannelsTable from './table-added-channels/table-added-channels';
import AvailableChannelsTable from './table-available-channels/table-available-channels';

import { formsIDS } from '../../../constants/formsIDS';

import { getStateModalSettingsReport } from './redux/selecteros';
import { getStatusFetchingReportEditor } from '../redux/selectors';
import { changeVisibilitySettingsReportModal } from './redux/actions';

export const ModalSettingsReport = ({ intl: { formatMessage } }) => {
  const dispatch = useDispatch();
  const confirmCreate = useSelector(getStatusFetchingReportEditor);
  const visible = useSelector(getStateModalSettingsReport);

  useEffect(() => {
    dispatch(changeVisibilitySettingsReportModal({ visible: true }));
  }, []);

  return (
    <AntModal
      title={formatMessage({ id: 'settingsReportForm.options' })}
      visible={visible}
      closable={false}
      width={930}
      confirmLoading={confirmCreate}
      onCancel={() =>
        dispatch(changeVisibilitySettingsReportModal({ visible: false }))
      }
      destroyOnClose
      // getContainer={() => document.getElementsByClassName('page-container')[0]}
      footer={[
        <ButtonStyled
          key="cancel"
          onClick={() =>
            dispatch(changeVisibilitySettingsReportModal({ visible: false }))
          }
        >
          {formatMessage({ id: 'btns.cancel' })}
        </ButtonStyled>,
        <ButtonStyled
          key="submit"
          type="primary"
          htmlType="submit"
          form={formsIDS.SETTINGS_REPORT}
        >
          {formatMessage({ id: 'btns.save' })}
        </ButtonStyled>
      ]}
    >
      <SettingsForm />
      <TreeDevices />
      <FlexGrid>
        <AvailableChannelsTable />
        <SelectedChannelsTable />
      </FlexGrid>
    </AntModal>
  );
};

ModalSettingsReport.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(ModalSettingsReport);
