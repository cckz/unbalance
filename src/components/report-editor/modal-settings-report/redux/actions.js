export const SETTINGS_REPORT_MODAL_VISIBILITY =
  'SETTINGS_REPORT_MODAL_VISIBILITY';

export const changeVisibilitySettingsReportModal = ({ visible }) => ({
  type: SETTINGS_REPORT_MODAL_VISIBILITY,
  visible
});
