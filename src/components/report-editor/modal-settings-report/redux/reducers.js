import { SETTINGS_REPORT_MODAL_VISIBILITY } from './actions';

export const settingsReportModal = (state = false, action) => {
  if (action.type === SETTINGS_REPORT_MODAL_VISIBILITY) return action.visible;
  return state;
};
