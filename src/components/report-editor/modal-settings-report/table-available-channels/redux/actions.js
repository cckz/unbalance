export const ADD_SELECTED_CHANNELS = 'ADD_SELECTED_CHANNELS';

export const addSelectedChannels = checkedIDs => ({
  type: ADD_SELECTED_CHANNELS,
  checkedIDs
});
