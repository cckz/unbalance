import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { func, shape } from 'prop-types';
import { injectIntl } from 'react-intl';
import styled from '@emotion/styled';

import Filter from '../filter-input';
import TitleTable from './table-title/table-title';
import { BaseTable } from '../../../base-table/base-table';

import {
  getSelectedChannels,
  getStatusFetchingReportEditor
} from '../../redux/selectors';

import { tablesIDs } from '../../../../constants/tables-ids';

const AvailableWrapStyled = styled.div`
  flex: 1;
  margin-inline-end: 1.3rem;
  margin-block-start: 2rem;
`;

const AvailableChannelsTable = ({ intl: { formatMessage } }) => {
  const selectedChannels = useSelector(getSelectedChannels);
  const isFetching = useSelector(getStatusFetchingReportEditor);

  const columns = useMemo(
    () =>
      ['device_name', 'name'].map(columName => ({
        title: formatMessage({ id: `channelsList.${columName}` }),
        dataIndex: columName,
        key: columName,
        width: columName === 'name' ? 116 : null,
        className: columName === 'name' && 'table__cell',
        sorter: (a, b) => {
          const first = a[columName].toLowerCase();
          const second = b[columName].toLowerCase();
          if (first < second) return -1;
          if (first > second) return 1;
          return 0;
        },
        onFilter: (value, { device_name, billing_group_name }) =>
          device_name.toLowerCase().includes(value.toLowerCase()) ||
          billing_group_name.toLowerCase().includes(value.toLowerCase()),
        filterDropdown:
          columName === 'device_name' ? props => <Filter {...props} /> : false,
        // FIXME скопированно со старого модуля
        render(text, record) {
          if (columName === 'device_name') {
            return record.device_id !== 0
              ? record.device_name
              : record.billing_group_name;
          }
          if (
            record.billing_group_id === 0 &&
            record.device_id !== 0 &&
            record.billing_group_name === ''
          ) {
            return record.name;
          }
          if (
            record.device_id !== 0 &&
            record.billing_group_id !== 0 &&
            record.billing_group_name !== ''
          ) {
            return record.billing_group_name;
          }
          return '—';
        }
      })),
    []
  );

  return (
    <AvailableWrapStyled>
      <TitleTable />
      <BaseTable
        tableID={tablesIDs.availableChannels}
        dataSource={selectedChannels}
        isFetching={isFetching}
        clientHeight={500}
        columns={columns}
      />
    </AvailableWrapStyled>
  );
};

AvailableChannelsTable.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(AvailableChannelsTable);
