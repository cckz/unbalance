import React from 'react';
import { injectIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { shape, func } from 'prop-types';
import { errorMessage } from '../../../../../messages/message';

import { addSelectedChannels } from '../redux/actions';

import {
  FlexGrid,
  ButtonStyled,
  TableTitle as TableTitleStyled
} from '../../../../common/styled';
import { tablesIDs } from '../../../../../constants/tables-ids';

const TitleTable = ({ intl: { formatMessage } }) => {
  const dispatch = useDispatch();
  const checkedRows = useSelector(
    state => state.checkedRows[tablesIDs.availableChannels]
  );
  const handleOnClick = () =>
    checkedRows.length
      ? dispatch(addSelectedChannels(checkedRows))
      : errorMessage(formatMessage({ id: 'messages.nonSelectedChannels' }));

  return (
    <FlexGrid>
      <TableTitleStyled>
        {formatMessage({ id: 'channelsList.availableChannels' })}
      </TableTitleStyled>
      <ButtonStyled
        type="primary"
        icon="plus"
        onClick={handleOnClick}
        data-testid="tree_devices"
      >
        {formatMessage({ id: 'channelsList.moveSelected' })}
      </ButtonStyled>
    </FlexGrid>
  );
};

TitleTable.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(TitleTable);
