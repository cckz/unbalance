export const DELETE_CHANNELS_FROM_ADDED_TABLE =
  'DELETE_CHANNELS_FROM_ADDED_TABLE';

export const REVERSE_CHANNEL = 'REVERSE_CHANNEL';

export const deleteChannels = ids => ({
  type: DELETE_CHANNELS_FROM_ADDED_TABLE,
  ids
});

export const reverseChannel = channel => ({
  type: REVERSE_CHANNEL,
  channel
});
