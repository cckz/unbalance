import React from 'react';
import { injectIntl } from 'react-intl';
import { shape, func } from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

import {
  FlexGrid,
  ButtonStyled,
  TableTitle as TableTitleStyled
} from '../../../../common/styled';
import { deleteChannels } from '../redux/actions';
import { errorMessage } from '../../../../../messages/message';

import { tablesIDs } from '../../../../../constants/tables-ids';

const TitleTable = ({ intl: { formatMessage } }) => {
  const dispatch = useDispatch();
  const checkedRows = useSelector(
    state => state.checkedRows[tablesIDs.addedChannels]
  );

  const handleOnClick = () =>
    checkedRows.length
      ? dispatch(deleteChannels(checkedRows))
      : errorMessage(formatMessage({ id: 'messages.nonSelectedChannels' }));

  return (
    <FlexGrid>
      <TableTitleStyled>
        {formatMessage({ id: 'channelsList.selectedChannels' })}
      </TableTitleStyled>
      <ButtonStyled type="danger" icon="delete" onClick={handleOnClick}>
        {formatMessage({ id: 'channelsList.deleteSelected' })}
      </ButtonStyled>
    </FlexGrid>
  );
};

TitleTable.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(TitleTable);
