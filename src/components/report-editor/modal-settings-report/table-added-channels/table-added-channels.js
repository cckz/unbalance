import React, { useMemo } from 'react';
import styled from '@emotion/styled';
import { injectIntl } from 'react-intl';
import { func, shape } from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { Icon, Button } from 'antd';

import { reverseChannel } from './redux/actions';
import Filter from '../filter-input';
import TitleTable from './table-title/table-title';
import { BaseTable } from '../../../base-table/base-table';

import {
  getAddedChannels,
  getReport,
  getStatusFetchingReportEditor
} from '../../redux/selectors';

import { errorMessage } from '../../../../messages/message';
import { tablesIDs } from '../../../../constants/tables-ids';

const SelectedWrapStyled = styled.div`
  flex: 1;
  margin-inline-start: 1.3rem;
  margin-block-start: 2rem;
`;

const AddedChannelsTable = ({ intl: { formatMessage } }) => {
  const dispatch = useDispatch();
  const { id } = useSelector(getReport);
  const addedChannels = useSelector(getAddedChannels);
  const isFetching = useSelector(getStatusFetchingReportEditor);

  const handleChangeTypeChannel = (e, record) => {
    e.currentTarget.blur();
    if (id) {
      dispatch(reverseChannel({ ...record, export: !record.export }));
    } else {
      errorMessage(formatMessage({ id: 'toastr.saveRequest' }));
    }
  };

  const getSorter = (a, b, columName) => {
    const first = a[columName].toLowerCase();
    const second = b[columName].toLowerCase();
    if (first < second) return -1;
    if (first > second) return 1;
    return 0;
  };

  const columns = useMemo(
    () =>
      ['device_name', 'name', 'considerHow'].map(columName => ({
        title: formatMessage({ id: `channelsList.${columName}` }),
        dataIndex: columName,
        key: columName,
        width: columName === 'device_name' ? false : 124,
        align: columName === 'considerHow' ? 'center' : 'left',
        className: columName === 'considerHow' && 'table__cell',
        onFilter: (value, { device_name, billing_group_name }) =>
          device_name.toLowerCase().includes(value.toLowerCase()) ||
          billing_group_name.toLowerCase().includes(value.toLowerCase()),
        filterDropdown:
          columName === 'device_name' ? props => <Filter {...props} /> : false,
        sorter:
          columName !== 'considerHow'
            ? (a, b) => getSorter(a, b, columName)
            : false,
        render(text, record) {
          if (columName === 'device_name') {
            return !record.billing_group_id
              ? record.device_name
              : record.billing_group_name;
          }
          if (columName === 'name') {
            return record.name ? record.name : '—';
          }
          return (
            <Button onClick={e => handleChangeTypeChannel(e, record)}>
              {record.export ? (
                <>
                  <Icon type="minus-square" theme="twoTone" /> Отдача
                </>
              ) : (
                <>
                  <Icon type="plus-square" theme="twoTone" /> Прием
                </>
              )}
            </Button>
          );
        }
      })),
    [addedChannels]
  );

  return (
    <SelectedWrapStyled>
      <TitleTable />
      <BaseTable
        tableID={tablesIDs.addedChannels}
        dataSource={addedChannels}
        isFetching={isFetching}
        clientHeight={500}
        columns={columns}
      />
    </SelectedWrapStyled>
  );
};

AddedChannelsTable.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(AddedChannelsTable);
