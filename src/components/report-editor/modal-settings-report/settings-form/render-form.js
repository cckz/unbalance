import React from 'react';
import { Form } from 'antd';
import styled from '@emotion/styled';

const StyledFormItem = styled(Form.Item)`
  ${({ width }) => width && `width: ${width}px`};
  margin-inline-end: 0rem !important;
  flex-wrap: wrap;
  .ant-form-item-label {
    padding-block-end: 0 !important;
    label {
      margin-block-end: 0 !important;
      font-weight: bold;
      font-size: 14px;
    }
  }
`;

export const RenderForm = ({ formFields, form }) => {
  return formFields.map(field => (
    <StyledFormItem
      {...field.layout}
      label={field.label}
      key={field.nameField}
      width={field.width}
      help=""
    >
      {form.getFieldDecorator(field.nameField, {
        initialValue: field.initial,
        rules: field.rules,
        placeholder: field.placeholder,
        valuePropName: field.valuePropName ? field.valuePropName : 'value'
      })(field.input)}
    </StyledFormItem>
  ));
};
