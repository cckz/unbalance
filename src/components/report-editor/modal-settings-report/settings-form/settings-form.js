import React, { memo, useMemo } from 'react';
import { injectIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { shape, any, objectOf, func } from 'prop-types';
import { Form, Icon, Input, Select, Spin, Switch } from 'antd';

import { RenderForm } from './render-form';
import { FormStyled } from '../../../common/styled';

import { getReport } from '../../redux/selectors';
import { formsIDS } from '../../../../constants/formsIDS';
import { errorMessage } from '../../../../messages/message';
import { changeVisibilitySettingsReportModal } from '../redux/actions';
import {
  createNewReportRequest,
  updateReportRequest
} from '../../redux/actions';

const SettingsForm = ({ form, intl: { formatMessage } }) => {
  const dispatch = useDispatch();
  const { validateFields } = form;
  const report = useSelector(getReport);
  const checkboxes = ['factory_number', 'ktt', 'ktn', 'km', 'ks'];
  const mainField = ['nameDevice', 'periodDefaultRequest'];

  const periods = [
    { value: '1d', label: formatMessage({ id: 'reportsList.day' }) },
    { value: '1m', label: formatMessage({ id: 'reportsList.month' }) }
  ];

  const validateNameField = (rule, value, callback) => {
    if (rule.field !== 'nameDevice') return callback();
    if (value) return callback();
    const errorMsg = `${formatMessage({
      id: 'messages.nameReportRequiredField'
    })}`;
    errorMessage(errorMsg);
    return callback(errorMsg);
  };

  const handleSubmit = e => {
    e.preventDefault();
    validateFields(
      (
        error,
        { nameDevice, km, ks, ktn, ktt, periodDefaultRequest, factory_number }
      ) => {
        if (error) {
          errorMessage(
            `${formatMessage({
              id: 'messages.nameReportRequiredField'
            })}`
          );
        } else {
          const params = {
            name: nameDevice,
            period: periodDefaultRequest,
            factory_number,
            ktt,
            ktn,
            km,
            ks
          };
          dispatch(
            report.id
              ? updateReportRequest(params, report.id)
              : createNewReportRequest(params)
          );
          dispatch(changeVisibilitySettingsReportModal({ visible: false }));
        }
      }
    );
  };

  const extractInitialValueField = field => {
    if (field === 'periodDefaultRequest') return report.period || '1d';
    if (field === 'nameDevice') return report.name;
    return Object.keys(report).length ? report[field] : true;
  };

  const getInputField = field => {
    if (field === 'nameDevice') return <Input />;
    return field === 'periodDefaultRequest' ? (
      <Select>
        {periods.map(period => (
          <Select.Option value={period.value} key={period.value}>
            {period.label}
          </Select.Option>
        ))}
      </Select>
    ) : (
      <Switch
        checkedChildren={<Icon type="check" />}
        unCheckedChildren={<Icon type="close" />}
      />
    );
  };

  const generateWidthField = field => {
    if (['nameDevice', 'periodDefaultRequest'].includes(field)) return 420;
    if (field === 'factory_number') return 186;
    if (field === 'ks') return 100;
    return 90;
  };

  const getLayout = field => {
    if (mainField.includes(field)) {
      return { labelCol: { span: 24 }, wrapperCol: { span: 24 } };
    }
    return field === 'factory_number'
      ? { labelCol: { span: 18 }, wrapperCol: { span: 6 } }
      : { labelCol: { span: 12 }, wrapperCol: { span: 12 } };
  };

  const renderFormFields = useMemo(
    () =>
      [...mainField, ...checkboxes].map(field => ({
        nameField: field,
        label: formatMessage({ id: `labels.${field}` }),
        initial: extractInitialValueField(field),
        width: generateWidthField(field),
        valuePropName: checkboxes.includes(field) ? 'checked' : 'value',
        input: getInputField(field),
        layout: getLayout(field),
        rules: [{ validator: validateNameField }]
      })),
    [report]
  );

  return (
    <Spin
      delay={300}
      spinning={false}
      indicator={<Icon type="loading" style={{ fontSize: 32 }} />}
    >
      <FormStyled
        id={formsIDS.SETTINGS_REPORT}
        onSubmit={handleSubmit}
        layout="inline"
      >
        <RenderForm formFields={renderFormFields} form={form} />
      </FormStyled>
    </Spin>
  );
};

SettingsForm.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired,
  form: objectOf(any).isRequired
};

const SettingsWrappedForm = Form.create({ name: formsIDS.SETTINGS_REPORT })(
  memo(SettingsForm)
);

export default injectIntl(SettingsWrappedForm);
