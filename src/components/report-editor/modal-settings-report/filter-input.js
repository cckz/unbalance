import React from 'react';
import { func, shape } from 'prop-types';
import styled from '@emotion/styled';
import { injectIntl } from 'react-intl';
import { StyledDebounceInput } from '../../common/styled';

const WrappFilter = styled.div`
  padding: 8px;
`;

export const FilterInput = ({
  setSelectedKeys,
  confirm,
  intl: { formatMessage }
}) => (
  <WrappFilter>
    <StyledDebounceInput
      minLength={0}
      debounceTimeout={800}
      onChange={e => {
        setSelectedKeys(e.target.value ? [e.target.value] : []);
        confirm();
      }}
      placeholder={formatMessage({ id: 'optionsSelector.enterTheName' })}
    />
  </WrappFilter>
);

FilterInput.propTypes = {
  setSelectedKeys: func.isRequired,
  confirm: func.isRequired,
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(FilterInput);
