import React, { useMemo } from 'react';
import styled from '@emotion/styled';
import { injectIntl } from 'react-intl';
import { HotTable } from '@handsontable/react';
import { func, string, shape } from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

import Handsontable from 'handsontable';
import { changeExcelReport } from '../redux/actions';
import { errorMessage } from '../../../messages/message';
import { getExcel, getBlockedRows } from './redux/selectors';
import { checkBoxes } from '../../../constants/hidden-colum-excel';
import { getClientHeight } from '../../../client-settings/selectors';

import 'handsontable/dist/handsontable.full.css';
import { getDisabledParameters } from '../redux/selectors';

const HotTableStyled = styled(HotTable)`
  .htDimmed {
    background: #eee;
  }

  .ht_master .wtHolder ::-webkit-scrollbar {
    width: 12px !important;
    height: 12px !important;
  }
`;

function firstRowRenderer(instance, td) {
  Handsontable.renderers.TextRenderer.apply(this, arguments);
  td.style.fontWeight = 'bold';
  td.style.background = '#eee';
}

const Excel = ({ intl: { formatMessage }, id }) => {
  const dispatch = useDispatch();
  const excel = useSelector(getExcel);
  const blockedRows = useSelector(getBlockedRows);
  const clientHeight = useSelector(getClientHeight);
  const disabledParameters = useSelector(getDisabledParameters);

  const hiddenColumns = useMemo(
    () => disabledParameters.map(parameter => checkBoxes[parameter]),
    [disabledParameters]
  );

  const mergeCells = useMemo(
    () =>
      excel.merged_cells
        ? excel.merged_cells.map(values => ({
            col: values.left_top.column,
            row: values.left_top.row,
            colspan: values.right_bottom.column - values.left_top.column + 1,
            rowspan: values.right_bottom.row - values.left_top.row + 1
          }))
        : [],
    [excel.merged_cells]
  );

  const cellsReadOnly = useMemo(() => {
    if (blockedRows) {
      const { start, count } = blockedRows;
      return Array.from({ length: start + count - start }, (v, k) => k + start);
    }
    return null;
  }, [blockedRows]);

  const getWidthCell = cellValue => {
    if (!cellValue) return 0;
    if (cellValue && cellValue.length <= 5) return 44;
    const cellWidth = cellValue.length * 7;
    return cellWidth > 400 ? 400 : cellWidth;
  };

  const generateColumnsWidth = useMemo(() => {
    if (!excel.rows) return [];
    return excel.rows[0].reduce((res, _, indexFirst) => {
      res[indexFirst] = 10;
      excel.rows.forEach((__, index) => {
        const cellValue = excel.rows[index][indexFirst];
        const width = getWidthCell(cellValue);
        if (res[indexFirst] < width) {
          res[indexFirst] = width;
        }
      });
      return res;
    }, []);
  }, [excel.rows]);

  const handleChangeCell = ([editCell]) => {
    if (!id)
      errorMessage(formatMessage({ id: 'toastr.customizeAndSaveRequest' }));
    else {
      const [row, column, oldValue, newValue] = editCell;
      if (oldValue !== newValue) {
        const changesCell = {
          [`${row}_${column}`]: {
            row,
            column: parseInt(column, 10),
            text: newValue
          }
        };
        dispatch(changeExcelReport(changesCell));
      }
    }
  };

  return (
    <>
      {excel.rows && (
        <HotTableStyled
          id="hot"
          settings={{
            data: excel.rows,
            rowHeaders: true,
            colHeaders: true,
            manualColumnResize: true,
            fixedRowsTop: 0,
            fixedColumnsLeft: 0,
            stretchH: 'all',
            colWidths: excel.rows && generateColumnsWidth,
            mergeCells,
            height: clientHeight - 20,
            cells(row, col) {
              const cellProperties = {};
              if (cellsReadOnly) {
                cellsReadOnly.forEach(numberRow => {
                  if (row === numberRow) {
                    cellProperties.readOnly = true;
                  }
                  if (hiddenColumns.includes(col)) {
                    cellProperties.renderer = firstRowRenderer;
                  }
                });
              }
              return cellProperties;
            },
            afterChange(changes) {
              if (changes && changes.length === 1) {
                handleChangeCell(changes);
              }
            }
          }}
        />
      )}
    </>
  );
};

Excel.propTypes = {
  id: string,
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

Excel.defaultProps = {
  id: ''
};

export default injectIntl(Excel);
