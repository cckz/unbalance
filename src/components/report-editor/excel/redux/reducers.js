import { combineReducers } from 'redux';
import { CHANGE_EXCEL_REPORT, REPORT_SUCCESS } from '../../redux/constants';
import { INITIAL_REPORT_EDITOR_SUCCESS } from '../../initialization/actions';

export const sheet = (state = {}, action) => {
  switch (action.type) {
    case REPORT_SUCCESS:
    case INITIAL_REPORT_EDITOR_SUCCESS:
      return action.sheet;
    default:
      return state;
  }
};

export const blockedRows = (state = {}, action) => {
  switch (action.type) {
    case REPORT_SUCCESS:
    case INITIAL_REPORT_EDITOR_SUCCESS:
      return action.blocked_rows;
    default:
      return state;
  }
};

export const changesExcelReport = (state = {}, action) => {
  if (action.type === CHANGE_EXCEL_REPORT) {
    return { ...state, ...action.changes };
  }
  return state;
};

export const excel = combineReducers({
  sheet,
  blockedRows,
  changesExcelReport
});
