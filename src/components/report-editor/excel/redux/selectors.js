export const getExcel = state => state.reportEditor.excel.sheet;
export const getBlockedRows = state => state.reportEditor.excel.blockedRows;
export const getChangesExcelReport = state =>
  state.reportEditor.excel.changesExcelReport;
