import React from 'react';
import styled from '@emotion/styled';
import { Link } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { keyframes } from '@emotion/core';
import { shape, func, string } from 'prop-types';

import {
  StyledTitle,
  ButtonStyled,
  StyledSubHeader,
  ButtonBackStyled,
  StyledActionSubHeader
} from '../../common/styled';

import { saveExcelReportRequest } from '../redux/actions';
import { changeVisibilitySettingsReportModal } from '../modal-settings-report/redux/actions';
import { getReport } from '../redux/selectors';
import { routers } from '../../../constants/routers';

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const AnimationButton = styled(ButtonStyled)`
  :hover svg {
    animation: ${rotate} infinite 2s linear;
  }
`;

export const SubHeaderReportEditor = ({ intl: { formatMessage }, id }) => {
  const dispatch = useDispatch();
  const { name } = useSelector(getReport);

  return (
    <StyledSubHeader>
      <Link to={routers.reports_list}>
        <ButtonBackStyled type="primary" icon="arrow-left" />
      </Link>
      <StyledTitle>
        {id
          ? formatMessage({ id: 'reportEditor.titles.edit' })
          : formatMessage({ id: 'reportEditor.titles.create' })}
        : {name}
      </StyledTitle>
      <StyledActionSubHeader>
        {id && (
          <ButtonStyled
            type="primary"
            icon="save"
            onClick={e => {
              e.currentTarget.blur();
              dispatch(saveExcelReportRequest());
            }}
          >
            {formatMessage({ id: 'reportEditor.btns.save' })}
          </ButtonStyled>
        )}
        <AnimationButton
          icon="setting"
          type="primary"
          onClick={e => {
            e.currentTarget.blur();
            dispatch(changeVisibilitySettingsReportModal({ visible: true }));
          }}
        >
          {formatMessage({ id: 'reportEditor.btns.сustomize' })}
        </AnimationButton>
      </StyledActionSubHeader>
    </StyledSubHeader>
  );
};

SubHeaderReportEditor.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired,
  id: string
};

SubHeaderReportEditor.defaultProps = {
  id: ''
};

export default injectIntl(SubHeaderReportEditor);
