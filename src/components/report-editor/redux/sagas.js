import { call, put, takeLatest, select } from 'redux-saga/effects';

import { API_PREFIX, fetchJSON } from '../../../redux/api';
import { intl } from '../../intl-global-provider/intl-global-provider';

import { getReport, getAddedChannels } from './selectors';
import { generateIDsForChannels } from '../initialization/sagas';
import { getChangesExcelReport } from '../excel/redux/selectors';
import { errorMessage, successMessage } from '../../../messages/message';
import {
  REPORT_REQUEST,
  UPDATE_REPORT_REQUEST,
  CREATE_NEW_REPORT_REQUEST,
  SAVE_CHANGES_EXCEL_REPORT_REQUEST
} from './constants';
import {
  createNewReportFailure,
  createNewReportSuccess,
  saveExcelReportFailure,
  saveExcelReportSuccess,
  updateReportFailure,
  updateReportSuccess,
  reportFailure,
  reportSuccess,
  reportRequest as reportRequestAction
} from './actions';

export function* requestReport({ id }) {
  try {
    const { report, blocked_rows, sheet } = yield call(
      fetchJSON,
      `${API_PREFIX}/read/${id}`
    );
    // у каналов нет id
    report.channels = generateIDsForChannels(report.channels);

    yield put(
      reportSuccess({
        report,
        blocked_rows,
        sheet
      })
    );
  } catch (error) {
    errorMessage(
      error.response.data
        ? error.response.data.message
        : `Status: ${error.response.status}`
    );
    yield put(
      reportFailure({
        message: error.response.data ? error.response.data.message : null,
        status: error.response.status
      })
    );
  }
}

export function* requestCreateNewReport({ params }) {
  const channels = yield select(getAddedChannels);
  try {
    const { id } = yield call(fetchJSON, `${API_PREFIX}/create`, {
      method: 'POST',
      data: JSON.stringify({ ...params, channels })
    });
    if (id) {
      yield put(createNewReportSuccess(id));
      successMessage(intl.formatMessage({ id: 'toastr.createSuccess' }));
    }
  } catch (error) {
    errorMessage(
      error.response.data
        ? error.response.data.message
        : `Status: ${error.response.status}`
    );
    yield put(
      createNewReportFailure({
        message: error.response.data ? error.response.data.message : null,
        status: error.response.status
      })
    );
  }
}

export function* requestUpdateCurrentReport({ params, id }) {
  const channels = yield select(getAddedChannels);
  try {
    yield call(fetchJSON, `${API_PREFIX}/update/${id}`, {
      method: 'PUT',
      data: { ...params, channels }
    });
    yield put(updateReportSuccess(id));
    yield put(reportRequestAction(id));
    successMessage(intl.formatMessage({ id: 'toastr.saveChanges' }));
  } catch (error) {
    errorMessage(
      error.response.data
        ? error.response.data.message
        : `Status: ${error.response.status}`
    );
    yield put(
      updateReportFailure({
        message: error.response.data ? error.response.data.message : null,
        status: error.response.status
      })
    );
  }
}

export function* requestSaveExcelChangesReport() {
  const { id } = yield select(getReport);
  const changes = yield select(getChangesExcelReport);
  const extractChangesFromObj = Object.values(changes);
  if (extractChangesFromObj.length) {
    try {
      yield call(fetchJSON, `${API_PREFIX}/write/${id}`, {
        method: 'POST',
        data: Object.values(changes)
      });
      yield put(saveExcelReportSuccess());
      successMessage(intl.formatMessage({ id: 'toastr.createSuccess' }));
    } catch (error) {
      errorMessage(
        error.response.data
          ? error.response.data.message
          : `Status: ${error.response.status}`
      );
      yield put(
        saveExcelReportFailure({
          message: error.response.data ? error.response.data.message : null,
          status: error.response.status
        })
      );
    }
  } else {
    errorMessage(intl.formatMessage({ id: 'toastr.nonChanges' }));
  }
}

export function* watchReportEditor() {
  yield takeLatest(REPORT_REQUEST, requestReport);
  yield takeLatest(UPDATE_REPORT_REQUEST, requestUpdateCurrentReport);
  yield takeLatest(CREATE_NEW_REPORT_REQUEST, requestCreateNewReport);
  yield takeLatest(
    SAVE_CHANGES_EXCEL_REPORT_REQUEST,
    requestSaveExcelChangesReport
  );
}
