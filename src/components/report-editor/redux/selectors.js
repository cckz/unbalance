export const getChannels = state => state.reportEditor.channels.allChannels;
export const getSelectedChannels = state =>
  state.reportEditor.channels.selectedChannels;
export const getAddedChannels = state =>
  state.reportEditor.channels.addedChannels;

export const getReport = state => state.reportEditor.report;

export const getDisabledParameters = state =>
  state.reportEditor.disabledParameters;
export const getStatusFetchingReportEditor = state =>
  state.reportEditor.isFetching;
