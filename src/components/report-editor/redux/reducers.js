import { combineReducers } from 'redux';
import uniqby from 'lodash.uniqby';

import { excel } from '../excel/redux/reducers';
import { treeDevices } from '../modal-settings-report/tree/redux/reducers';
import { CHANGE_CHECKED_NODES } from '../modal-settings-report/tree/redux/actions';
import { INITIAL_STATEMENTS_LIST_SUCCESS } from '../../statements-list/initialization/actions';
import { ADD_SELECTED_CHANNELS } from '../modal-settings-report/table-available-channels/redux/actions';
import {
  DELETE_CHANNELS_FROM_ADDED_TABLE,
  REVERSE_CHANNEL
} from '../modal-settings-report/table-added-channels/redux/actions';

import {
  INITIAL_REPORT_EDITOR_REQUEST,
  INITIAL_REPORT_EDITOR_SUCCESS,
  INITIAL_REPORT_EDITOR_FAILURE
} from '../initialization/actions';

import {
  CREATE_NEW_REPORT_REQUEST,
  CREATE_NEW_REPORT_FAILURE,
  CREATE_NEW_REPORT_SUCCESS,
  REPORT_SUCCESS
} from './constants';

const report = (state = {}, action) => {
  switch (action.type) {
    case REPORT_SUCCESS:
    case INITIAL_REPORT_EDITOR_SUCCESS:
    case INITIAL_STATEMENTS_LIST_SUCCESS:
      return action.report;
    case CREATE_NEW_REPORT_SUCCESS:
      return { idNewReport: action.idNewReport };
    default:
      return state;
  }
};

const disabledParameters = (state = [], action) => {
  switch (action.type) {
    case REPORT_SUCCESS:
    case INITIAL_REPORT_EDITOR_SUCCESS:
    case INITIAL_STATEMENTS_LIST_SUCCESS:
      return Object.keys(action.report).filter(
        row => action.report[row] === false
      );
    default:
      return state;
  }
};

const isFetching = (state = false, action) => {
  switch (action.type) {
    case INITIAL_REPORT_EDITOR_REQUEST:
    case CREATE_NEW_REPORT_REQUEST:
      return true;
    case INITIAL_REPORT_EDITOR_FAILURE:
    case INITIAL_REPORT_EDITOR_SUCCESS:
    case CREATE_NEW_REPORT_FAILURE:
    case CREATE_NEW_REPORT_SUCCESS:
      return false;
    default:
      return state;
  }
};

const initStateChannels = {
  allChannels: [],
  selectedChannels: [],
  addedChannels: []
};

const channels = (state = initStateChannels, action) => {
  switch (action.type) {
    case INITIAL_REPORT_EDITOR_SUCCESS:
      return {
        ...state,
        allChannels: action.channels,
        addedChannels: action.report.channels || []
      };
    // FIXME подобной логике тут не место
    case CHANGE_CHECKED_NODES:
      return {
        ...state,
        selectedChannels: state.allChannels.filter(channel =>
          action.selectedNodes.some(
            objectId =>
              channel.device_id === +objectId ||
              objectId === `bg-${channel.billing_group_id}`
          )
        )
      };
    case ADD_SELECTED_CHANNELS: {
      return {
        ...state,
        addedChannels: uniqby(
          [
            ...state.addedChannels,
            ...state.selectedChannels.filter(channel =>
              action.checkedIDs.includes(channel.id)
            )
          ],
          'id'
        )
      };
    }
    case DELETE_CHANNELS_FROM_ADDED_TABLE:
      return {
        ...state,
        addedChannels: state.addedChannels.filter(
          channel => !action.ids.includes(channel.id)
        )
      };
    case REVERSE_CHANNEL:
      return {
        ...state,
        addedChannels: state.addedChannels.map(channel =>
          channel.id === action.channel.id ? action.channel : channel
        )
      };
    default:
      return state;
  }
};

export const reportEditor = combineReducers({
  excel,
  report,
  channels,
  isFetching,
  treeDevices,
  disabledParameters
});
