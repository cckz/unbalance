import {
  REPORT_FAILURE,
  REPORT_REQUEST,
  REPORT_SUCCESS,
  CHANGE_EXCEL_REPORT,
  UPDATE_REPORT_FAILURE,
  UPDATE_REPORT_REQUEST,
  UPDATE_REPORT_SUCCESS,
  CREATE_NEW_REPORT_SUCCESS,
  CREATE_NEW_REPORT_FAILURE,
  CREATE_NEW_REPORT_REQUEST,
  SAVE_CHANGES_EXCEL_REPORT_FAILURE,
  SAVE_CHANGES_EXCEL_REPORT_REQUEST,
  SAVE_CHANGES_EXCEL_REPORT_SUCCESS
} from './constants';

export const updateReportRequest = (params, id) => ({
  type: UPDATE_REPORT_REQUEST,
  params,
  id
});

export const updateReportSuccess = () => ({
  type: UPDATE_REPORT_SUCCESS
});

export const updateReportFailure = error => ({
  type: UPDATE_REPORT_FAILURE,
  error
});

export const changeExcelReport = changes => ({
  type: CHANGE_EXCEL_REPORT,
  changes
});

export const createNewReportRequest = params => ({
  type: CREATE_NEW_REPORT_REQUEST,
  params
});

export const createNewReportSuccess = idNewReport => ({
  type: CREATE_NEW_REPORT_SUCCESS,
  idNewReport
});

export const createNewReportFailure = error => ({
  type: CREATE_NEW_REPORT_FAILURE,
  error
});

export const saveExcelReportRequest = () => ({
  type: SAVE_CHANGES_EXCEL_REPORT_REQUEST
});

export const saveExcelReportSuccess = idNewReport => ({
  type: SAVE_CHANGES_EXCEL_REPORT_SUCCESS,
  idNewReport
});

export const saveExcelReportFailure = error => ({
  type: SAVE_CHANGES_EXCEL_REPORT_FAILURE,
  error
});

export const reportRequest = id => ({
  type: REPORT_REQUEST,
  id
});

export const reportSuccess = ({ report, blocked_rows, sheet }) => ({
  type: REPORT_SUCCESS,
  report,
  blocked_rows,
  sheet
});

export const reportFailure = error => ({
  type: REPORT_FAILURE,
  error
});
