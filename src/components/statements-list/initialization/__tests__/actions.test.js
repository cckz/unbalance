import configureStore from 'redux-mock-store';

import {
  initialStatementsListFailure,
  INITIAL_STATEMENTS_LIST_FAILURE,
  INITIAL_STATEMENTS_LIST_REQUEST,
  INITIAL_STATEMENTS_LIST_SUCCESS,
  initialStatementsListRequest,
  initialStatementsListSuccess
} from '../actions';

import { error404 } from '../../../../../tests/__mock__/errors.mock';
import { reportsList } from '../../../../../tests/__mock__/reports.mock';

const mockStore = configureStore();
const store = mockStore();

describe('initial reports-list', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('report-list request action', () => {
    const expectedActions = [
      {
        type: INITIAL_STATEMENTS_LIST_REQUEST,
        clientHeight: 700,
        reportID: 1
      }
    ];
    store.dispatch(
      initialStatementsListRequest({
        clientHeight: 700,
        reportID: 1
      })
    );
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('report-list success action', () => {
    const expectedActions = [
      {
        type: INITIAL_STATEMENTS_LIST_SUCCESS,
        sheet: [],
        report: reportsList[0],
        statementsList: [],
        blocked_rows: [],
        clientHeight: 700
      }
    ];
    store.dispatch(
      initialStatementsListSuccess({
        sheet: [],
        report: reportsList[0],
        statementsList: [],
        blocked_rows: [],
        clientHeight: 700
      })
    );
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('report-list failure action', () => {
    const expectedActions = [
      {
        type: INITIAL_STATEMENTS_LIST_FAILURE,
        error: error404
      }
    ];
    store.dispatch(initialStatementsListFailure(error404));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
