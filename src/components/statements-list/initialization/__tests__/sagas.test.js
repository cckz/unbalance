import { expectSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';

import * as matchers from 'redux-saga-test-plan/matchers';
import rootSaga from '../../../../redux/sagas';
import { API_PREFIX, fetchJSON } from '../../../../redux/api';

import {
  initialStatementsListFailure,
  initialStatementsListSuccess,
  initialStatementsListRequest
} from '../actions';
import { statementsList } from '../../../../../tests/__mock__/statementsList.mock';

import {
  error404,
  errorResponse
} from '../../../../../tests/__mock__/errors.mock';
import { initState } from '../../../../../tests/__mock__/init-state.mock';
import { reportRead } from '../../../../../tests/__mock__/report.mock';

describe('reports-list initialization sagas', () => {
  const reportID = '942';
  const urlStatements = `${API_PREFIX}/table/{"id": 942, "begin":"2019-06-10 00:00:00","end":"2019-06-17 00:00:00"}`;
  const urlRead = `${API_PREFIX}/read/${reportID}`;

  it('Запрашиваем данные для инициализации: succeess', () => {
    return (
      expectSaga(rootSaga)
        .withState(initState)
        .provide([
          [matchers.call.fn(fetchJSON, urlStatements), statementsList],
          [matchers.call.fn(fetchJSON, urlRead), reportRead]
        ])

        // undefined в реальной саге при тесте не может деструктуризировать
        .put(
          initialStatementsListSuccess({
            sheet: undefined,
            blocked_rows: undefined,
            report: undefined,
            clientHeight: 700,
            statementsList
          })
        )
        .dispatch(
          initialStatementsListRequest({ clientHeight: 700, reportID: '942' })
        )
        .silentRun()
    );
  });

  it('Запрашиваем данные для инициализации: failure', () => {
    return expectSaga(rootSaga)
      .withState(initState)
      .provide([
        [matchers.call.fn(fetchJSON, urlStatements), throwError(errorResponse)],
        [matchers.call.fn(fetchJSON, urlRead), throwError(errorResponse)]
      ])
      .put(initialStatementsListFailure(error404))
      .dispatch(
        initialStatementsListRequest({ clientHeight: 700, reportID: '942' })
      )
      .silentRun();
  });
});
