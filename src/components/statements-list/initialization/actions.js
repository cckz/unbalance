export const INITIAL_STATEMENTS_LIST_REQUEST =
  'INITIAL_STATEMENTS_LIST_REQUEST';
export const INITIAL_STATEMENTS_LIST_SUCCESS =
  'INITIAL_STATEMENTS_LIST_SUCCESS';
export const INITIAL_STATEMENTS_LIST_FAILURE =
  'INITIAL_STATEMENTS_LIST_FAILURE';

export const initialStatementsListRequest = ({ clientHeight, reportID }) => ({
  type: INITIAL_STATEMENTS_LIST_REQUEST,
  clientHeight,
  reportID
});

export const initialStatementsListSuccess = ({
  sheet,
  report,
  statementsList,
  blocked_rows,
  clientHeight
}) => ({
  type: INITIAL_STATEMENTS_LIST_SUCCESS,
  sheet,
  report,
  statementsList,
  blocked_rows,
  clientHeight
});

export const initialStatementsListFailure = error => ({
  type: INITIAL_STATEMENTS_LIST_FAILURE,
  error
});
