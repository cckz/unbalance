import { call, put, all, select } from 'redux-saga/effects';

import { getRangeDate } from '../subheader/date-picker/redux/selectors';

import {
  initialStatementsListSuccess,
  initialStatementsListFailure
} from './actions';
import { API_PREFIX, fetchJSON } from '../../../redux/api';
import { errorMessage } from '../../../messages/message';

export function* requestInitialStatementsList({ clientHeight, reportID }) {
  try {
    const { begin, end } = yield select(getRangeDate);
    const [statementsList, { report, blocked_rows, sheet }] = yield all([
      yield call(
        fetchJSON,
        `${API_PREFIX}/table/{"id": ${reportID}, "begin":"${begin.format(
          'YYYY-MM-DD HH:mm:ss'
        )}","end":"${end.format('YYYY-MM-DD HH:mm:ss')}"}`
      ),
      yield call(fetchJSON, `${API_PREFIX}/read/${reportID}`)
    ]);
    yield put(
      initialStatementsListSuccess({
        sheet,
        report,
        statementsList,
        blocked_rows,
        clientHeight
      })
    );
  } catch (error) {
    errorMessage(
      error.response.data
        ? error.response.data.message
        : `Status: ${error.response.status}`
    );
    yield put(
      initialStatementsListFailure({
        message: error.response.data ? error.response.data.message : null,
        status: error.response.status
      })
    );
  }
}
