import {
  isFetching as isFetchingReportsReducer,
  list as statementsListReducer
} from '../reducers';
import {
  statementsListSuccess,
  statementsListRequest,
  statementsListFailure
} from '../actions';

import {
  initialStatementsListFailure,
  initialStatementsListRequest,
  initialStatementsListSuccess
} from '../../initialization/actions';

import { error404 } from '../../../../../tests/__mock__/errors.mock';
import { statementsList } from '../../../../../tests/__mock__/statementsList.mock';

describe('statements-list reducers', () => {
  // Reports List
  it('initial state списка ведомости', () => {
    expect(statementsListReducer(undefined, {})).toEqual({});
  });
  it('Запрос ведомости: success ', () => {
    expect(
      statementsListReducer([], statementsListSuccess({ statementsList }))
    ).toEqual(statementsList);
  });
  it('Запрос данных при инициализации: success ', () => {
    expect(
      statementsListReducer(
        [],
        initialStatementsListSuccess({
          sheet: {},
          report: {},
          statementsList,
          blocked_rows: {},
          clientHeight: 700
        })
      )
    ).toEqual(statementsList);
  });

  // isFetching
  it('initialState состояния isFetching', () => {
    expect(isFetchingReportsReducer(undefined, {})).toEqual(false);
  });
  it('статус isFetching = true', () => {
    expect(isFetchingReportsReducer(false, statementsListRequest())).toEqual(
      true
    );
    expect(
      isFetchingReportsReducer(
        false,
        initialStatementsListRequest({ clientHeight: 700, reportID: '900' })
      )
    ).toEqual(true);
  });
  it('статус isFetching = false', () => {
    expect(
      isFetchingReportsReducer(true, statementsListSuccess({ statementsList }))
    ).toEqual(false);
    expect(
      isFetchingReportsReducer(true, statementsListFailure(error404))
    ).toEqual(false);

    expect(
      isFetchingReportsReducer(
        true,
        initialStatementsListSuccess({
          sheet: {},
          report: {},
          statementsList,
          blocked_rows: {},
          clientHeight: 700
        })
      )
    ).toEqual(false);
    expect(
      isFetchingReportsReducer(true, initialStatementsListFailure(error404))
    ).toEqual(false);
  });
});
