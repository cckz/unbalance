import { expectSaga } from 'redux-saga-test-plan';
import { throwError } from 'redux-saga-test-plan/providers';

import * as matchers from 'redux-saga-test-plan/matchers';
import rootSaga from '../../../../redux/sagas';
import { API_PREFIX, fetchJSON } from '../../../../redux/api';

import {
  statementsListFailure,
  statementsListSuccess,
  statementsListRequest
} from '../actions';
import { statementsList } from '../../../../../tests/__mock__/statementsList.mock';

import {
  error404,
  errorResponse
} from '../../../../../tests/__mock__/errors.mock';
import { initState } from '../../../../../tests/__mock__/init-state.mock';

describe('reports-list initialization sagas', () => {
  const urlStatements = `${API_PREFIX}/table/{"id": 942, "begin":"2019-06-10 00:00:00","end":"2019-06-17 00:00:00"}`;

  it('Запрашиваем данные: succeess', () => {
    return expectSaga(rootSaga)
      .withState(initState)
      .provide([[matchers.call.fn(fetchJSON, urlStatements), statementsList]])
      .put(
        statementsListSuccess({
          statementsList
        })
      )
      .dispatch(statementsListRequest())
      .silentRun();
  });

  it('Запрашиваем данные: failure', () => {
    return expectSaga(rootSaga)
      .withState(initState)
      .provide([
        [matchers.call.fn(fetchJSON, urlStatements), throwError(errorResponse)]
      ])
      .put(statementsListFailure(error404))
      .dispatch(statementsListRequest())
      .silentRun();
  });
});
