import configureStore from 'redux-mock-store';

import {
  STATEMENTS_LIST_SUCCESS,
  STATEMENTS_LIST_FAILURE,
  STATEMENTS_LIST_REQUEST,
  statementsListFailure,
  statementsListRequest,
  statementsListSuccess
} from '../actions';

import { error404 } from '../../../../../tests/__mock__/errors.mock';
import { statementsList } from '../../../../../tests/__mock__/statementsList.mock';

const mockStore = configureStore();
const store = mockStore();

describe('statements-list actions', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('Запрос ведомости: request', () => {
    const expectedActions = [
      {
        type: STATEMENTS_LIST_REQUEST
      }
    ];
    store.dispatch(statementsListRequest());
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('Запрос ведомости: success', () => {
    const expectedActions = [
      {
        type: STATEMENTS_LIST_SUCCESS,
        statementsList
      }
    ];
    store.dispatch(statementsListSuccess({ statementsList }));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('Запрос ведомости: failure', () => {
    const expectedActions = [
      {
        type: STATEMENTS_LIST_FAILURE,
        error: error404
      }
    ];
    store.dispatch(statementsListFailure(error404));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
