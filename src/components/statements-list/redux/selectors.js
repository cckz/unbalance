export const getStatementsList = state => state.statementsList.list;
export const getStatusFetchingStatementsList = state =>
  state.statementsList.isFetching;
