import { combineReducers } from 'redux';
import { datePicker } from '../subheader/date-picker/redux/reducers';

import {
  INITIAL_STATEMENTS_LIST_REQUEST,
  INITIAL_STATEMENTS_LIST_FAILURE,
  INITIAL_STATEMENTS_LIST_SUCCESS
} from '../initialization/actions';
import {STATEMENTS_LIST_FAILURE, STATEMENTS_LIST_REQUEST, STATEMENTS_LIST_SUCCESS} from './actions';

export const list = (state = {}, action) => {
  switch (action.type) {
    case STATEMENTS_LIST_SUCCESS:
    case INITIAL_STATEMENTS_LIST_SUCCESS:
      return action.statementsList;
    default:
      return state;
  }
};

export const isFetching = (state = false, action) => {
  switch (action.type) {
    case STATEMENTS_LIST_REQUEST:
    case INITIAL_STATEMENTS_LIST_REQUEST:
      return true;
    case STATEMENTS_LIST_SUCCESS:
    case STATEMENTS_LIST_FAILURE:
    case INITIAL_STATEMENTS_LIST_FAILURE:
    case INITIAL_STATEMENTS_LIST_SUCCESS:
      return false;
    default:
      return state;
  }
};

export const statementsList = combineReducers({
  list,
  isFetching,
  datePicker
});
