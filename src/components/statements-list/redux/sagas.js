import { call, put, select, takeLatest } from 'redux-saga/effects';

import { errorMessage } from '../../../messages/message';
import { API_PREFIX, fetchJSON } from '../../../redux/api';

import { getReport } from '../../report-editor/redux/selectors';
import { getRangeDate } from '../subheader/date-picker/redux/selectors';
import { CHANGE_DATE_RANGE } from '../subheader/date-picker/redux/actions';

import {
  statementsListRequest,
  statementsListFailure,
  statementsListSuccess,
  STATEMENTS_LIST_REQUEST
} from './actions';

export function* requestStatementsList() {
  try {
    const { begin, end } = yield select(getRangeDate);
    const { id } = yield select(getReport);

    const statementsList = yield call(
      fetchJSON,
      `${API_PREFIX}/table/{"id": ${id}, "begin":"${begin.format(
        'YYYY-MM-DD HH:mm:ss'
      )}","end":"${end.format('YYYY-MM-DD HH:mm:ss')}"}`
    );
    yield put(
      statementsListSuccess({
        statementsList
      })
    );
  } catch (error) {
    errorMessage(
      error.response.data
        ? error.response.data.message
        : `Status: ${error.response.status}`
    );
    yield put(
      statementsListFailure({
        message: error.response.data ? error.response.data.message : null,
        status: error.response.status
      })
    );
  }
}

export function* requestStatementsListWithNewRangeDate() {
  yield put(statementsListRequest());
}

export function* watchStatementsList() {
  yield takeLatest(STATEMENTS_LIST_REQUEST, requestStatementsList);
  yield takeLatest(CHANGE_DATE_RANGE, requestStatementsListWithNewRangeDate);
}
