export const STATEMENTS_LIST_REQUEST = 'STATEMENTS_LIST_REQUEST';
export const STATEMENTS_LIST_SUCCESS = 'STATEMENTS_LIST_SUCCESS';
export const STATEMENTS_LIST_FAILURE = 'STATEMENTS_LIST_FAILURE';

export const statementsListRequest = () => ({
  type: STATEMENTS_LIST_REQUEST
});

export const statementsListSuccess = ({ statementsList }) => ({
  type: STATEMENTS_LIST_SUCCESS,
  statementsList
});

export const statementsListFailure = error => ({
  type: STATEMENTS_LIST_FAILURE,
  error
});
