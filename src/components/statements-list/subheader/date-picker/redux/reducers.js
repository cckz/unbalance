import moment from 'moment';
import { CHANGE_DATE_RANGE } from './actions';

export const initialState = {
  begin: moment()
    .subtract(1, 'days')
    .startOf('day'),
  end: moment().startOf('day')
};

export const datePicker = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_DATE_RANGE: {
      const { begin, end } = action;
      return {
        begin,
        end
      };
    }

    default:
      return state;
  }
};
