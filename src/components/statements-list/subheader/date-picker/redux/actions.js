export const CHANGE_DATE_RANGE = 'CHANGE_DATE_RANGE';

export const changeDateRange = (begin, end) => ({
  type: CHANGE_DATE_RANGE,
  begin,
  end
});
