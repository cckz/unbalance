import React, { useMemo } from 'react';
import moment from 'moment';
import { DatePicker } from 'antd';
import { injectIntl } from 'react-intl';
import { func, shape, string } from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

import { getRangeDate } from './redux/selectors';
import { changeDateRange } from './redux/actions';

const { RangePicker } = DatePicker;

const DatesPicker = ({ intl: { locale, formatMessage } }) => {
  const dispatch = useDispatch();
  const rangesDate = useSelector(getRangeDate);
  const defaultRange = [rangesDate.begin, rangesDate.end];

  const staticRanges = useMemo(
    () => ({
      [formatMessage({ id: 'date.yesterday' })]: defaultRange,
      [formatMessage({ id: 'date.lastSevenDays' })]: [
        moment()
          .subtract(7, 'days')
          .startOf('day'),
        moment().startOf('day')
      ],
      [formatMessage({ id: 'date.lastThirtyDays' })]: [
        moment()
          .subtract(30, 'days')
          .startOf('day'),
        moment().startOf('day')
      ],
      [formatMessage({ id: 'date.currentMonth' })]: [
        moment()
          .subtract('month')
          .startOf('month'),
        moment().startOf('day')
      ],

      [formatMessage({ id: 'date.previousMonth' })]: [
        moment()
          .subtract(1, 'month')
          .startOf('month'),
        moment()
          .subtract(1, 'month')
          .endOf('month')
          .startOf('day')
      ]
    }),
    [rangesDate]
  );
  moment.locale(locale);

  return (
    <RangePicker
      allowClear={false}
      showTime={{ format: 'HH:mm' }}
      format="D MMMM YYYY HH:mm"
      disabledDate={current => current > moment().endOf('day')}
      value={defaultRange}
      ranges={staticRanges}
      onChange={([begin, end]) => dispatch(changeDateRange(begin, end))}
    />
  );
};

DatesPicker.propTypes = {
  intl: shape({
    locale: string.isRequired,
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(DatesPicker);
