import React from 'react';
import { injectIntl } from 'react-intl';
import { shape, func, string } from 'prop-types';

import { useSelector } from 'react-redux';
import {
  StyledTitle,
  ButtonStyled,
  StyledSubHeader,
  ButtonBackStyled,
  StyledActionSubHeader
} from '../../common/styled';
import DatesPicker from './date-picker/date-picker';

import { getRangeDate } from './date-picker/redux/selectors';
import { getReport } from '../../report-editor/redux/selectors';

import { API_PREFIX } from '../../../redux/api';

export const SubHeaderStatementsList = ({
  intl: { formatMessage },
  reportID
}) => {
  const { begin, end } = useSelector(getRangeDate);
  const { name } = useSelector(getReport);
  return (
    <StyledSubHeader>
      <ButtonBackStyled
        type="primary"
        icon="arrow-left"
        onClick={() => window.history.back()}
      />
      <StyledTitle>
        {formatMessage({ id: 'statementsList.title' })}: {name}
      </StyledTitle>
      <StyledActionSubHeader>
        <ButtonStyled
          icon="download"
          type="primary"
          onClick={e => e.currentTarget.blur()}
          href={`${API_PREFIX}/download/{"id": ${reportID},"begin":"${begin.format(
            'YYYY-MM-DD HH:mm:ss'
          )}","end":"${end.format('YYYY-MM-DD HH:mm:ss')}"}`}
        >
          {formatMessage({ id: 'btns.export' })}
        </ButtonStyled>
        <DatesPicker />
      </StyledActionSubHeader>
    </StyledSubHeader>
  );
};

SubHeaderStatementsList.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired,
  reportID: string.isRequired
};

export default injectIntl(SubHeaderStatementsList);
