import React, { useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { injectIntl } from 'react-intl';
import { Helmet } from 'react-helmet';
import { func, shape, objectOf, any } from 'prop-types';

import { offset } from '../../constants/offset';
import { initialStatementsListRequest } from './initialization/actions';
import { changeClientContentSize } from '../../client-settings/action';

import SubHeader from './subheader/subheader';
import { ContentItem } from '../common/styled';
import StatementsListTable from './statements-list-table/statements-list-table';

const StatementsList = ({ intl: { formatMessage }, match }) => {
  const contentRef = useRef(null);
  const dispatch = useDispatch();
  useEffect(() => {
    if (contentRef.current && contentRef.current.clientHeight > 0) {
      dispatch(
        initialStatementsListRequest({
          clientHeight: contentRef.current.clientHeight - offset,
          reportID: match.params.id
        })
      );
    }
    const changeSizes = () =>
      dispatch(
        changeClientContentSize(contentRef.current.clientHeight - offset)
      );
    window.addEventListener('resize', changeSizes);
    return () => window.removeEventListener('resize', changeSizes);
  }, [contentRef, window.innerHeight]);

  return (
    <>
      <Helmet title={formatMessage({ id: 'statementsList.titleWithSedmax' })} />
      <SubHeader reportID={match.params.id} />
      <ContentItem ref={contentRef}>
        <StatementsListTable />
      </ContentItem>
    </>
  );
};

StatementsList.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired,
  match: objectOf(any).isRequired
};

export default injectIntl(StatementsList);
