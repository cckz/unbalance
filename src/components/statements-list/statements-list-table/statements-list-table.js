import React, { useMemo } from 'react';
import { injectIntl } from 'react-intl';
import { func, shape } from 'prop-types';
import { useSelector } from 'react-redux';
import { Typography } from 'antd';

import { BaseTable } from '../../base-table/base-table';

import { getClientHeight } from '../../../client-settings/selectors';
import {
  getStatementsList,
  getStatusFetchingStatementsList
} from '../redux/selectors';
import { getDisabledParameters } from '../../report-editor/redux/selectors';
import { tablesIDs } from '../../../constants/tables-ids';

const { Text } = Typography;

const StatementsListTable = ({ intl: { formatMessage } }) => {
  const clientHeight = useSelector(getClientHeight);
  const statementsList = useSelector(getStatementsList);
  const disabledParameters = useSelector(getDisabledParameters);
  const isFetching = useSelector(getStatusFetchingStatementsList);

  const { table, units, total } = statementsList;
  const getColumnWidth = columName => {
    if (columName === 'name') return false;
    if (['ktt', 'ktn', 'km', 'ks'].includes(columName)) {
      return 70;
    }
    return 180;
  };

  const getTitleColumn = columName => {
    if (!['begin', 'end', 'diff', 'result'].includes(columName)) {
      return formatMessage({ id: `labels.${columName}` });
    }
    return `${formatMessage({ id: `labels.${columName}` })}  ${units}`;
  };

  const getSorter = (a, b, columName) => {
    if (['name', 'active', 'export'].includes(columName)) {
      const first = a[columName].toLowerCase();
      const second = b[columName].toLowerCase();
      if (first < second) return -1;
      if (first > second) return 1;
      return 0;
    }
    return a[columName] - b[columName];
  };

  const columns = useMemo(
    () =>
      [
        'name',
        'factory_number',
        'active',
        'export',
        'begin',
        'end',
        'diff',
        'ktt',
        'ktn',
        'km',
        'ks',
        'result'
      ]
        .filter(columName => !disabledParameters.includes(columName))
        .map(columName => ({
          title: getTitleColumn(columName),
          dataIndex: columName,
          key: columName,
          width: getColumnWidth(columName),
          className: columName === 'result' && 'table__cell',
          sorter: (a, b) => getSorter(a, b, columName)
        })),
    [disabledParameters]
  );

  const tableWithUniqueKey = useMemo(() => {
    if (!table) return [];
    return table.map((row, index) => ({ ...row, index }));
  }, [table]);

  return (
    <BaseTable
      tableID={tablesIDs.statementsList}
      dataSource={tableWithUniqueKey}
      columns={table ? columns : []}
      clientHeight={clientHeight}
      isCheckableTable={false}
      isFetching={isFetching}
      rowKey="index"
      footer={() =>
        table && (
          <Text strong>
            {formatMessage({ id: 'statementsList.total' })} : {total}, {units}
          </Text>
        )
      }
    />
  );
};

StatementsListTable.propTypes = {
  intl: shape({
    formatMessage: func.isRequired
  }).isRequired
};

export default injectIntl(StatementsListTable);
