import axios from 'axios';

export const API_PREFIX = '/sedmax/web/reports/unbalance';
export const fetchJSON = async (url, options = {}) => {
  const { data } = await axios(url, options);
  return data;
};
