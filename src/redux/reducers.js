import { combineReducers } from 'redux';

import { clientHeight } from '../client-settings/reducers';

import { checkedRows } from '../components/base-table/redux/reducers';
import { reports } from '../components/reports-list/redux/reducers';
import { reportEditor } from '../components/report-editor/redux/reducers';
import { statementsList } from '../components/statements-list/redux/reducers';
import { copyReportModal } from '../components/reports-list/modal-copy-report/redux/reducer';
import { settingsReportModal } from '../components/report-editor/modal-settings-report/redux/reducers';

const modalsState = combineReducers({
  settingsReportModal,
  copyReportModal
});

export const rootReducer = combineReducers({
  reports,
  reportEditor,
  statementsList,
  modalsState,
  checkedRows,
  clientHeight
});
