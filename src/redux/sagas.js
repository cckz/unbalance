import { spawn, takeLatest } from 'redux-saga/effects';

import { messages } from '../messages/sagas-confirm';

import { watchReports } from '../components/reports-list/redux/sagas';
import { watchReportEditor } from '../components/report-editor/redux/sagas';
import { watchStatementsList } from '../components/statements-list/redux/sagas';
import { requestInitiaReportsList } from '../components/reports-list/initialization/sagas';
import { INITIAL_REPORTS_REQUEST } from '../components/reports-list/initialization/actions';
import { requestInitialReportEditor } from '../components/report-editor/initialization/sagas';
import { requestInitialStatementsList } from '../components/statements-list/initialization/sagas';
import { INITIAL_REPORT_EDITOR_REQUEST } from '../components/report-editor/initialization/actions';
import { INITIAL_STATEMENTS_LIST_REQUEST } from '../components/statements-list/initialization/actions';

function* watchInitialApp() {
  yield takeLatest(INITIAL_REPORTS_REQUEST, requestInitiaReportsList);
  yield takeLatest(INITIAL_REPORT_EDITOR_REQUEST, requestInitialReportEditor);
  yield takeLatest(
    INITIAL_STATEMENTS_LIST_REQUEST,
    requestInitialStatementsList
  );
}

export default function* rootSaga() {
  yield spawn(messages);
  yield spawn(watchReports);
  yield spawn(watchInitialApp);
  yield spawn(watchReportEditor);
  yield spawn(watchStatementsList);
}
