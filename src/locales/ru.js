export const russian = {
  ru: {
    accessIsDenied: 'Доступ запрещен! Сервис авторизации не доступен.',
    noDataText: 'Нет данных',
    reportsList: {
      title: 'Список отчетов',
      titleWithSedmax: 'Отчеты по учету электроэнергии | SEDMAX',
      btns: {
        add: 'Добавить отчет',
        delete: 'Удалить выбранное',
        edit: 'Редактировать',
        copy: 'Копировать'
      },
      day: 'День',
      month: 'Месяц'
    },
    reportEditor: {
      titles: {
        edit: 'Редактирование шаблона',
        create: 'Создание шаблона'
      },
      titlesWithSedmax: {
        edit: 'Редактирование шаблона | SEDMAX',
        create: 'Создание шаблона | SEDMAX'
      },
      btns: {
        save: 'Сохранить отчет',
        сustomize: 'Настроить'
      },
      nameReport: 'Наименование отчета'
    },
    statementsList: {
      title: 'Ведомость показаний приборов учета',
      titleWithSedmax: 'Ведомость показаний приборов учета | SEDMAX',
      total: 'Итого'
    },
    settingsReportForm: {
      options: 'Настройки отчета'
    },
    optionsSelector: {
      title: 'Выбор каналов для отчета',
      enterTheName: 'Введите наименование'
    },
    channelsList: {
      availableChannels: 'Доступные каналы',
      moveSelected: 'Переместить выбранное',
      selectAllChannels: 'Выбрать все каналы',
      clearAll: 'Очистить все',
      enterTheTitle: 'Введите название',
      device_name: 'Точка учета',
      name: 'Канал учета',
      selectedChannels: 'Выбранные каналы',
      considerHow: 'Учитывать как',
      deleteSelected: 'Удалить выбранные'
    },
    date: {
      from: 'Дата начала: {from}',
      to: 'Дата окончания: {to}',
      title: 'Выберите диапазон дат',
      today: 'Сегодня',
      yesterday: 'Вчера',
      lastSevenDays: 'Последние 7 дней',
      lastThirtyDays: 'Последние 30 дней',
      currentMonth: 'Текущий месяц',
      previousMonth: 'Предыдущий месяц',
      apply: 'Применить',
      close: 'Закрыть'
    },
    btns: {
      save: 'Сохранить',
      apply: 'Применить',
      cancel: 'Отменить',
      confirm: 'Да',
      close: 'Закрыть',
      export: 'Экспорт',
      confirm_yes: 'Да',
      confirm_no: 'Нет'
    },
    labels: {
      id: 'ID',
      nameDevice: 'Наименование',
      period: 'Период',
      actions: 'Действия',
      name: 'Наименование прибора',
      serialNumber: 'Заводской номер прибора учета',
      active: 'Изм-ая величина',
      export: 'Направление перетока', // directionOverflow
      begin: 'Показания на начало периода',
      end: 'Показания на конец периода',
      diff: 'Разность показаний приборов учета',
      ktt: 'Ктт',
      ktn: 'Ктн',
      km: 'Ксч',
      ks: 'Кобщ',
      result: 'Количество электроэнергии, учтенной приборами учета',
      periodDefaultRequest: 'Период запроса (по умолчанию)',
      factory_number: 'Серийный номер'
    },
    toastr: {
      nonChanges: 'Изменений нет',
      deleteSuccess: 'Отчеты успешно удалены',
      singleDeleteSuccess: 'Отчет успешно удален',
      copySuccess: 'Отчет успешно скопирован',
      createSuccess: 'Отчет успешно создан',
      noSelectedError: 'Не выбрано ни одного отчета',
      fillingWarning: 'Заполните обязательные поля',
      customizeAndSaveRequest:
        'Настройте и сохраните отчет, чтобы его редактировать',
      dataUpdated: 'Данные обновлены',
      saveChanges: 'Изменения успешно сохранены',
      saveRequest: 'Сохраните отчет, чтобы вносить изменения в каналы'
    },
    messages: {
      nonSelectedChannels: 'Не выбранно ни одного канала',
      copyReport: 'Копирование отчета',
      confirm: 'Подтвердите изменения',
      copyRequest: 'Введите наименование нового отчета',
      nameReportRequiredField: 'Наименование отчета не может быть пустым',
      deleteRequest: 'Вы действительно хотите удалить отчеты',
      notCreatedReports: 'Не создано ни одного отчета',
      selectedPeriodRequest: 'Выберите период'
    }
  }
};
