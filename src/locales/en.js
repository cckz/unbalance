export const english = {
  en: {
    accessIsDenied:
      'Access is denied! The authorization service is not available.',
    noDataText: 'There is no data to display',
    reportsList: {
      title: 'List of reports',
      titleWithSedmax: 'Electric power reports| SEDMAX',
      btns: {
        add: 'Add report',
        delete: 'Delete selected',
        edit: 'Edit',
        copy: 'Copy'
      },
      day: 'Day',
      month: 'Month'
    },
    reporsEditor: {
      titles: {
        edit: 'Edit report',
        create: 'Create report'
      },
      titlesWithSedmax: {
        edit: 'Edit report | SEDMAX',
        create: 'Create report | SEDMAX'
      },
      btns: {
        save: 'Save report',
        сustomize: 'Configure'
      },
      nameReport: 'Report title'
    },
    statementsList: {
      title: 'Report',
      titleWithSedmax: 'Report | SEDMAX',
      total: 'Total'
    },
    settingsReportForm: {
      options: 'Report settings'
    },
    optionsSelector: {
      title: 'Selecting channels for the report',
      enterTheName: 'Enter the name'
    },
    channelsList: {
      availableChannels: 'Available channels',
      moveSelected: 'Move selected',
      selectAllChannels: 'Select all channels',
      clearAll: 'Clear all',
      enterTheTitle: 'Enter the title',
      device_name: 'Point metering',
      name: 'Metering channel',
      selectedChannels: 'Selected channels',
      considerHow: 'Consider how',
      deleteSelected: 'Delete selected'
    },
    date: {
      from: 'Begin: {from}',
      to: 'End: {to}',
      title: 'Select date range',
      today: 'Today',
      yesterday: 'Yesterday',
      lastSevenDays: 'Last 7 Days',
      lastThirtyDays: 'Last 30 Days',
      currentMonth: 'Current Month',
      previousMonth: 'Previous Month',
      apply: 'Apply',
      close: 'Close'
    },
    btns: {
      save: 'Save',
      apply: 'Apply',
      cancel: 'Cancel',
      confirm: 'Yes',
      close: 'Close',
      export: 'Export',
      confirm_yes: 'Yes',
      confirm_no: 'No'
    },
    labels: {
      copyReport: 'Копирование отчета',
      id: 'ID',
      nameDevice: 'Name',
      period: 'Period',
      actions: 'Actions',
      name: 'Name of the device',
      serialNumber: 'Factory number of the device',
      active: 'Type of power',
      export: 'Direction of the flow',
      begin: 'Indications at the beginning of the period',
      end: 'Indications at the end of the period',
      diff: 'Difference in meter by period',
      ktt: 'Kct',
      ktn: 'Kvt',
      km: 'Km',
      ks: 'Ktot',
      result: 'The amount of electricity metered by the device',
      periodDefaultRequest: 'Query period (default)',
      factory_number: 'Serial number'
    },
    toastr: {
      nonChanges: 'No change',
      singleDeleteSuccess: 'Report were successfully deleted',
      deleteSuccess: 'Reports were successfully deleted',
      copySuccess: 'Report was successfully copied',
      createSuccess: 'Report was successfully created',
      noSelectedError: 'No reports selected',
      fillingWarning: 'Please, fill required fields',
      customizeAndSaveRequest: 'Configure and save the report to edit it',
      dataUpdated: 'Data updated',
      saveChanges: 'Changes were successfully saved',
      saveRequest: 'Save the report to make changes to the channels'
    },
    messages: {
      nonSelectedChannels: 'No channel selected',
      copyReport: 'Copy report',
      confirm: 'Confirm changes',
      copyRequest: 'Enter the name of the new report',
      nameReportRequiredField: 'Report name cannot be empty',
      deleteRequest: 'Do you really want to delete reports?',
      notCreatedReports: 'No reports created',
      selectedPeriodRequest: 'Select a period'
    }
  }
};
