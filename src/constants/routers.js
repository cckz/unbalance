export const routers = {
  reports_list: '/index',
  create_template: '/new',
  info: '/info',
  view: '/view'
};
