export const tablesIDs = {
  reportsList: 'reportsList',
  addedChannels: 'addedChannels',
  statementsList: 'statementsList',
  availableChannels: 'availableChannels'
};
