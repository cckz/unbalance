import React from 'react';
import ReactDOM from 'react-dom';
import { LocaleProvider } from 'antd';
import { Provider } from 'react-redux';
import ru from 'react-intl/locale-data/ru';
import en from 'react-intl/locale-data/en';
import ruRU from 'antd/lib/locale-provider/ru_RU';
import enGB from 'antd/lib/locale-provider/en_GB';
import { addLocaleData, IntlProvider } from 'react-intl';

import store from './redux/store';
import App from './components/app';
import { russian } from './locales/ru';
import { english } from './locales/en';
import { getCookie } from './helpers/getCookie';
import { flattenMessages } from './helpers/flattenMessages';

import IntlGlobalProvider from './components/intl-global-provider/intl-global-provider';

addLocaleData([...en, ...ru]);

const antdLocales = {
  ru: ruRU,
  en: enGB
};

const translations = { ...russian, ...english };
const locale = getCookie('lang') || 'ru';

ReactDOM.render(
  <IntlProvider
    locale={locale}
    messages={flattenMessages(translations[locale])}
  >
    <IntlGlobalProvider>
      <LocaleProvider locale={antdLocales[locale]}>
        <Provider store={store}>
          <App />
        </Provider>
      </LocaleProvider>
    </IntlGlobalProvider>
  </IntlProvider>,
  document.getElementById('root')
);
