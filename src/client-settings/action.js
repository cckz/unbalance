export const CHANGE_CLIENT_CONTENT_SIZE = 'CHANGE_CLIENT_CONTENT_SIZE';

export const changeClientContentSize = clientHeight => ({
  type: CHANGE_CLIENT_CONTENT_SIZE,
  clientHeight
});
