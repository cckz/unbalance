import { CHANGE_CLIENT_CONTENT_SIZE } from './action';
import { INITIAL_REPORTS_SUCCESS } from '../components/reports-list/initialization/actions';
import { INITIAL_REPORT_EDITOR_SUCCESS } from '../components/report-editor/initialization/actions';
import { INITIAL_STATEMENTS_LIST_SUCCESS } from '../components/statements-list/initialization/actions';

export const clientHeight = (state = 0, action) => {
  switch (action.type) {
    case INITIAL_REPORTS_SUCCESS:
    case CHANGE_CLIENT_CONTENT_SIZE:
    case INITIAL_REPORT_EDITOR_SUCCESS:
    case INITIAL_STATEMENTS_LIST_SUCCESS:
      return action.clientHeight;
    default:
      return state;
  }
};
