import configureStore from 'redux-mock-store';

import { changeClientContentSize, CHANGE_CLIENT_CONTENT_SIZE } from '../action';

const mockStore = configureStore();
const store = mockStore();

describe('clientSettings actions', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('handles sets requests', () => {
    const expectedActions = [
      {
        type: CHANGE_CLIENT_CONTENT_SIZE,
        clientHeight: 200
      }
    ];
    store.dispatch(changeClientContentSize(200));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
