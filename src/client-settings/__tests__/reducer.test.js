import configureStore from 'redux-mock-store';

import { clientHeight as clientHeightReducer } from '../reducers';
import { CHANGE_CLIENT_CONTENT_SIZE } from '../action';
import { INITIAL_REPORTS_SUCCESS } from '../../components/reports-list/initialization/actions';
import { INITIAL_REPORT_EDITOR_SUCCESS } from '../../components/report-editor/initialization/actions';
import { INITIAL_STATEMENTS_LIST_SUCCESS } from '../../components/statements-list/initialization/actions';

const mockStore = configureStore();
const store = mockStore();

describe('client-settings reducers', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('initialState clientHeight', () => {
    expect(clientHeightReducer(undefined, {})).toEqual(0);
  });
  it('Проверяем высоту рабочего блока: clientHeight', () => {
    expect(
      clientHeightReducer(0, {
        type: CHANGE_CLIENT_CONTENT_SIZE,
        clientHeight: 795
      })
    ).toEqual(795);
  });
  expect(
    clientHeightReducer(0, {
      type: INITIAL_REPORTS_SUCCESS,
      clientHeight: 795
    })
  ).toEqual(795);
  expect(
    clientHeightReducer(0, {
      type: CHANGE_CLIENT_CONTENT_SIZE,
      clientHeight: 795
    })
  ).toEqual(795);


  expect(
    clientHeightReducer(0, {
      type: INITIAL_REPORT_EDITOR_SUCCESS,
      clientHeight: 795
    })
  ).toEqual(795);
  expect(
    clientHeightReducer(0, {
      type: INITIAL_STATEMENTS_LIST_SUCCESS,
      clientHeight: 795
    })
  ).toEqual(795);
});
