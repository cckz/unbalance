import { message } from 'antd';

message.config({
  top: 3,
  duration: 2,
  maxCount: 1
});

export const errorMessage = errMessage => message.error(`${errMessage}`);
export const successMessage = succMessage => message.success(`${succMessage}`);
