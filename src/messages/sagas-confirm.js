import { takeLatest, call, put } from 'redux-saga/effects';
import { Modal } from 'antd';

import { SHOW_CONFIRMATION, confirmYes, confirmNo } from './actions';
import { intl } from '../components/intl-global-provider/intl-global-provider';

export const confirmAction = confirmData =>
  new Promise((resolve, reject) => {
    Modal.confirm({
      getContainer: () => document.getElementsByClassName('page-container')[0],
      title: intl.formatMessage({ id: confirmData.title }),
      content:
        confirmData.message && intl.formatMessage({ id: confirmData.message }),
      okText: `Да`,
      cancelText: 'Нет',
      zIndex: 1002,
      onOk() {
        resolve();
      },
      onCancel() {
        reject();
      }
    });
  });

function* showConfirmation({ confirmData }) {
  try {
    yield call(confirmAction, confirmData);
    yield put(confirmYes());
  } catch (error) {
    yield put(confirmNo());
  }
}

export function* messages() {
  yield takeLatest(SHOW_CONFIRMATION, showConfirmation);
}
