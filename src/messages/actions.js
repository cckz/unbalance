export const SHOW_CONFIRMATION = 'SHOW_CONFIRMATION';

export const CONFIRM_YES = 'CONFIRM_YES';
export const CONFIRM_NO = 'CONFIRM_NO';

export const showConfirmation = confirmData => ({
  type: SHOW_CONFIRMATION,
  confirmData
});

export const confirmYes = () => ({ type: CONFIRM_YES });
export const confirmNo = () => ({ type: CONFIRM_NO });
