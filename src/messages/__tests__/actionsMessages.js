import configureStore from 'redux-mock-store';
import {
  CONFIRM_NO,
  CONFIRM_YES,
  SHOW_CONFIRMATION,
  confirmNo,
  confirmYes,
  showConfirmation
} from '../actions';

const mockStore = configureStore();
const store = mockStore();

describe('Messages actions', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('handles show confirm', () => {
    const expectedActions = [
      {
        type: SHOW_CONFIRMATION,
        confirmData: { title: 'messages.confirm', message: '' }
      }
    ];
    store.dispatch(
      showConfirmation({ title: 'messages.confirm', message: '' })
    );
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('handles confirm yes', () => {
    const expectedActions = [
      {
        type: CONFIRM_YES
      }
    ];
    store.dispatch(confirmYes());
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('handles confirm no', () => {
    const expectedActions = [
      {
        type: CONFIRM_NO
      }
    ];
    store.dispatch(confirmNo());
    expect(store.getActions()).toEqual(expectedActions);
  });
});
