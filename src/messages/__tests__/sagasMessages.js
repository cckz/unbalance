import { call } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';

import { confirmAction, messages } from '../sagas-confirm';
import { confirmYes, showConfirmation, confirmNo } from '../actions';

describe('EditorSets sagas', () => {
  it('sagas messages confirm yes', () => {
    return expectSaga(messages)
      .provide([
        [
          call(confirmAction, { title: 'messages.confirm', message: '' }),
          Promise.resolve()
        ]
      ])
      .put(confirmYes())
      .dispatch(showConfirmation({ title: 'messages.confirm', message: '' }))
      .silentRun();
  });

  it('sagas messages confirm yes', () => {
    return expectSaga(messages)
      .provide([
        [
          call(confirmAction, { title: 'messages.confirm', message: '' }),
          Promise.reject()
        ]
      ])
      .put(confirmNo())
      .dispatch(showConfirmation({ title: 'messages.confirm', message: '' }))
      .silentRun();
  });
});
