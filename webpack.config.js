/* eslint-disable */
const isProduction = process.argv[process.argv.indexOf('--mode') + 1] === 'production';
const report = false

const webpackConfig = isProduction ? require('./config/productionConfig') : require('./config/devConfig') ;

if (report) {
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
    .BundleAnalyzerPlugin;
  webpackConfig.plugins.push(new BundleAnalyzerPlugin());
}

module.exports = webpackConfig;