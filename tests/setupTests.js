import React from 'react';
import 'jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { render } from '@testing-library/react';

import { createStore } from 'redux';
import { russian } from '../src/locales/ru';
import { english } from '../src/locales/en';
import { routers } from '../src/constants/routers';
import { rootReducer } from '../src/redux/reducers';

import { flattenMessages } from '../src/helpers/flattenMessages';
import IntlGlobalProvider from '../src/components/intl-global-provider/intl-global-provider';

const translations = { ...russian, ...english };
const locale = 'ru';

const intlProvider = new IntlProvider({
  locale,
  messages: flattenMessages(translations[locale])
});
const { intl } = intlProvider.getChildContext();

const renderWithIntl = ui => {
  const expectedComponent = {
    ...render(
      <IntlProvider
        locale={locale}
        messages={flattenMessages(translations[locale])}
      >
        <IntlGlobalProvider>{ui}</IntlGlobalProvider>
      </IntlProvider>
      // container ? { container: document.body } : {}
    )
  };
  return expectedComponent;
};

const renderWithIntlAndRedux = (
  ui,
  { initialState, store = createStore(rootReducer, initialState) } = {}
  // container = null
) => {
  const expectedComponent = {
    ...render(
      <IntlProvider
        locale={locale}
        messages={flattenMessages(translations[locale])}
      >
        <IntlGlobalProvider>
          <Provider store={store}>{ui}</Provider>
        </IntlGlobalProvider>
      </IntlProvider>
      // container ? { container: document.body } : {}
    ),
    store
  };
  // для диспатча в компонентах и ререндеринга с учетом изменений, когда руки дойдут
  expectedComponent.rerenderWithRedux = (el, nextState) => {
    if (nextState) {
      store.replaceReducer(() => nextState);
      // store.dispatch({ type: TREE_VISIBILITY });
      // store.replaceReducer(rootReducer);
    }
    return renderWithIntlAndRedux(el, { store }, expectedComponent.rerender);
  };
  return expectedComponent;
};

export const renderWithRouter = (
  ui,
  {
    route = routers.templatesMenager,
    history = createMemoryHistory({ initialEntries: [route] })
  } = {}
) => {
  return {
    ...render(<Router history={history}>{ui}</Router>),
    // adding `history` to the returned utilities to allow us
    // to reference it in our tests (just try to avoid using
    // this to test implementation details).
    history
  };
};

export {
  render,
  cleanup,
  fireEvent,
  waitForElement
} from '@testing-library/react';
export { renderWithIntlAndRedux, renderWithIntl, intl };
