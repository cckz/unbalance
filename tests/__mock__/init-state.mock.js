import moment from 'moment';

export const initState = {
  reports: { list: [], isFetching: false },
  reportEditor: {
    excel: { sheet: {}, blockedRows: {}, changesExcelReport: {} },
    report: {},
    channels: { allChannels: [], selectedChannels: [], addedChannels: [] },
    isFetching: false,
    treeDevices: { checkedNodes: [], flatStructure: [] },
    disabledParameters: []
  },
  statementsList: {
    list: {},
    isFetching: false,
    datePicker: {
      begin: moment()
        .subtract(1, 'days')
        .startOf('day'),
      end: moment().startOf('day')
    }
  },
  modalsState: {
    settingsReportModal: false,
    copyReportModal: { visible: false, id: null }
  },
  tables: {
    baseTable: { checkedRows: [212], pageSizeTables: 13 },
    addedChennelsTable: { checkedRowsAddedChannels: [], pageSizeTables: 13 }
  },
  clientHeight: 0
};
