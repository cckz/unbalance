export const statementsList = {
  units: 'кВт*ч',
  table: [
    {
      name: 'СЭТ-4ТМ.02M.02',
      factory_number: '',
      ktt: 1,
      ktn: 1,
      km: 1,
      ks: 1,
      active: 'активная',
      export: 'прием',
      begin: 'н/д',
      end: 'н/д',
      diff: 'н/д',
      result: 'н/д'
    },
    {
      name: 'СЭТ-4ТМ.02M.02',
      factory_number: '',
      ktt: 1,
      ktn: 1,
      km: 1,
      ks: 1,
      active: 'активная',
      export: 'отдача',
      begin: 'н/д',
      end: 'н/д',
      diff: 'н/д',
      result: 'н/д'
    },
    {
      name: 'СЭТ-4ТМ.02M.02',
      factory_number: '',
      ktt: 1,
      ktn: 1,
      km: 1,
      ks: 1,
      active: 'реактивная',
      export: 'прием',
      begin: 'н/д',
      end: 'н/д',
      diff: 'н/д',
      result: 'н/д'
    }
  ],
  total: 'н/д'
};
