export const error404 = {
  status: 404,
  message: null
};

export const errorResponse = {
  response: {
    data: {
      message: null
    },
    status: 404
  }
};
