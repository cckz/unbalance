export const reportsList = [
  {
    id: 212,
    name: '12123',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 213,
    name: '222',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 214,
    name: '23233',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 215,
    name: 'Меркурий230',
    channels: [
      {
        device_id: 234,
        device_name: 'Меркурий 230',
        device_factory_number: '21931744',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 202,
        code: 'ra_imp',
        name: 'A+',
        export: false
      },
      {
        device_id: 234,
        device_name: 'Меркурий 230',
        device_factory_number: '21931744',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 202,
        code: 'ra_exp',
        name: 'A-',
        export: true
      },
      {
        device_id: 234,
        device_name: 'Меркурий 230',
        device_factory_number: '21931744',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 202,
        code: 'rr_imp',
        name: 'R+',
        export: false
      },
      {
        device_id: 234,
        device_name: 'Меркурий 230',
        device_factory_number: '21931744',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 202,
        code: 'rr_exp',
        name: 'R-',
        export: true
      }
    ],
    period: '1m',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 220,
    name: 'Еще один тестовый отчет',
    channels: [
      {
        device_id: 267,
        device_name: '720',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_imp',
        name: 'A+',
        export: false
      },
      {
        device_id: 267,
        device_name: '720',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_exp',
        name: 'A-',
        export: true
      },
      {
        device_id: 267,
        device_name: '720',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'rr_imp',
        name: 'R+',
        export: false
      },
      {
        device_id: 267,
        device_name: '720',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'rr_exp',
        name: 'R-',
        export: true
      },
      {
        device_id: 268,
        device_name: '720-30',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_imp',
        name: 'A+',
        export: false
      },
      {
        device_id: 268,
        device_name: '720-30',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_exp',
        name: 'A-',
        export: true
      },
      {
        device_id: 268,
        device_name: '720-30',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'rr_imp',
        name: 'R+',
        export: false
      },
      {
        device_id: 268,
        device_name: '720-30',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'rr_exp',
        name: 'R-',
        export: true
      }
    ],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 221,
    name: 'test',
    channels: [
      {
        device_id: 17,
        device_name: 'СЭТ-4ТМ.02M.02',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 13,
        code: 'ra_imp',
        name: 'A+',
        export: false
      }
    ],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 222,
    name: '3 min',
    channels: [
      {
        device_id: 267,
        device_name: '720',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_imp',
        name: 'A+',
        export: false
      }
    ],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 810,
    name: 'autotest_report_copy',
    channels: [
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_imp',
        name: 'A+',
        export: false
      },
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_exp',
        name: 'A-',
        export: false
      },
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'rr_imp',
        name: 'R+',
        export: false
      },
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'rr_exp',
        name: 'R-',
        export: false
      }
    ],
    period: '1d',
    factory_number: true,
    ktt: false,
    ktn: false,
    km: false,
    ks: false
  },
  {
    id: 819,
    name: ' ',
    channels: [
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_imp',
        name: 'A+',
        export: false
      },
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_exp',
        name: 'A-',
        export: false
      },
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'rr_imp',
        name: 'R+',
        export: false
      },
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'rr_exp',
        name: 'R-',
        export: false
      }
    ],
    period: '1d',
    factory_number: true,
    ktt: false,
    ktn: false,
    km: false,
    ks: false
  },
  {
    id: 821,
    name: '',
    channels: [
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_imp',
        name: 'A+',
        export: false
      },
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'ra_exp',
        name: 'A-',
        export: false
      },
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'rr_imp',
        name: 'R+',
        export: false
      },
      {
        device_id: 1057,
        device_name: 'auto_test1',
        device_factory_number: '1002110',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 7,
        code: 'rr_exp',
        name: 'R-',
        export: false
      }
    ],
    period: '1d',
    factory_number: true,
    ktt: false,
    ktn: false,
    km: false,
    ks: false
  },
  {
    id: 822,
    name: ' ',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 823,
    name: ' ',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 824,
    name: '',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 825,
    name: '',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 826,
    name: '123123',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 827,
    name: '',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 828,
    name: ' ',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 829,
    name: 'ываыа',
    channels: [],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 837,
    name: 'asdasda',
    channels: [
      {
        device_id: 1060,
        device_name: 'Satec PM180 (э/э - 3мин,30мин, а теперь 30 и 3)',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 18,
        protocol_id: 7,
        code: 'ra_imp',
        name: 'A+',
        export: false
      },
      {
        device_id: 1060,
        device_name: 'Satec PM180 (э/э - 3мин,30мин, а теперь 30 и 3)',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 18,
        protocol_id: 7,
        code: 'ra_imp',
        name: 'A+',
        export: false
      }
    ],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  },
  {
    id: 838,
    name: 'Тест. Минимум полей',
    channels: [
      {
        device_id: 0,
        device_name: '',
        device_factory_number: '',
        billing_group_id: 12,
        billing_group_name: 'not found',
        object_id: 0,
        protocol_id: 0,
        code: '',
        name: '',
        export: false
      },
      {
        device_id: 17,
        device_name: 'СЭТ-4ТМ.02M.02',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 13,
        code: 'ra_imp',
        name: 'A+',
        export: false
      },
      {
        device_id: 17,
        device_name: 'СЭТ-4ТМ.02M.02',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 13,
        code: 'ra_exp',
        name: 'A-',
        export: true
      },
      {
        device_id: 17,
        device_name: 'СЭТ-4ТМ.02M.02',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 13,
        code: 'rr_imp',
        name: 'R+',
        export: false
      }
    ],
    period: '1d',
    factory_number: false,
    ktt: false,
    ktn: false,
    km: false,
    ks: false
  },
  {
    id: 839,
    name: 'Тест. Максимум полей',
    channels: [
      {
        device_id: 0,
        device_name: '',
        device_factory_number: '',
        billing_group_id: 12,
        billing_group_name: 'not found',
        object_id: 0,
        protocol_id: 0,
        code: '',
        name: '',
        export: false
      },
      {
        device_id: 17,
        device_name: 'СЭТ-4ТМ.02M.02',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 13,
        code: 'ra_imp',
        name: 'A+',
        export: false
      },
      {
        device_id: 17,
        device_name: 'СЭТ-4ТМ.02M.02',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 13,
        code: 'ra_exp',
        name: 'A-',
        export: true
      },
      {
        device_id: 17,
        device_name: 'СЭТ-4ТМ.02M.02',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 13,
        code: 'rr_imp',
        name: 'R+',
        export: false
      }
    ],
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true
  }
];
