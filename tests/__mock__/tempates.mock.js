import { Template } from '../../src/components/common/types';

export const templatesMock: Template[] = [
  {
    id: 271,
    name: 'test_real',
    filename: '271_2018-04-23.xlsx',
    type: 'month',
    group: '',
    metrology_control: false,
    user_id: 1,
    user_name: 'sed',
    created_at: '2018-04-23 10:02:40',
    updated_at: '2019-02-26 12:04:30',
    auto_generation: {
      current_period: false,
      current_period_cron_rule: '* * * * * *',
      previous_period: false,
      previous_period_cron_rule: '* * * * * *',
      copy_files: false,
      network_addresses: [],
      is_send_email: false,
      email: { server_id: 1, recipients: [] }
    }
  },
  {
    id: 291,
    name: 'test_for_null',
    filename: '291_2019-02-11.xlsx',
    type: 'month',
    group: '',
    metrology_control: false,
    user_id: 1,
    user_name: 'sed',
    created_at: '2019-02-11 13:26:31',
    updated_at: '2019-02-11 13:26:31',
    auto_generation: {
      current_period: false,
      current_period_cron_rule: '* */5 * * * *',
      previous_period: false,
      previous_period_cron_rule: '0 */1 * * * *',
      copy_files: false,
      network_addresses: [],
      is_send_email: false,
      email: { server_id: 1, recipients: [] }
    }
  },
  {
    id: 292,
    name: 'test',
    filename: '292_2019-02-14.xlsx',
    type: 'year',
    group: 'test',
    metrology_control: false,
    user_id: 1,
    user_name: 'sed',
    created_at: '2019-02-14 14:03:20',
    updated_at: '2019-02-14 14:03:20',
    auto_generation: {
      current_period: false,
      current_period_cron_rule: '* * * * * *',
      previous_period: false,
      previous_period_cron_rule: '* * * * * *',
      copy_files: false,
      network_addresses: [],
      is_send_email: false,
      email: { server_id: 0, recipients: [] }
    }
  },
  {
    id: 294,
    name: 'test_real',
    filename: '294_2019-02-14.xlsx',
    type: 'year',
    group: '',
    metrology_control: false,
    user_id: 1,
    user_name: 'sed',
    created_at: '2019-02-14 15:23:35',
    updated_at: '2019-02-14 15:23:35',
    auto_generation: {
      current_period: false,
      current_period_cron_rule: '* * * * * *',
      previous_period: false,
      previous_period_cron_rule: '* * * * * *',
      copy_files: false,
      network_addresses: [],
      is_send_email: false,
      email: { server_id: 1, recipients: ['jkjk@dsgf.sdf'] }
    }
  },
  {
    id: 295,
    name: 'test_real1',
    filename: '295_2019-02-14.xlsx',
    type: 'year',
    group: '',
    metrology_control: false,
    user_id: 1,
    user_name: 'sed',
    created_at: '2019-02-14 15:25:42',
    updated_at: '2019-02-27 18:22:22',
    auto_generation: {
      current_period: false,
      current_period_cron_rule: '* * * * * *',
      previous_period: false,
      previous_period_cron_rule: '* * * * * *',
      copy_files: false,
      network_addresses: [],
      is_send_email: false,
      email: { server_id: 1, recipients: [] }
    }
  },
  {
    id: 320,
    name: 'Месячный отчет по ЭЭ',
    filename: '320_2019-02-27.xlsx',
    type: 'month',
    group: '',
    metrology_control: true,
    user_id: 1,
    user_name: 'sed',
    created_at: '2019-02-27 17:04:25',
    updated_at: '2019-02-27 17:04:25',
    auto_generation: {
      current_period: false,
      current_period_cron_rule: '',
      previous_period: false,
      previous_period_cron_rule: '',
      copy_files: false,
      network_addresses: [],
      is_send_email: false,
      email: { server_id: 0, recipients: [] }
    }
  }
];
