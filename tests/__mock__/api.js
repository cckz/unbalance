import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const mock = new MockAdapter(axios);

export const fetchPostFailure = url => {
  mock.reset();
    const mock = new MockAdapter(axios);
  return mock.onPost(url).reply(404);
};
