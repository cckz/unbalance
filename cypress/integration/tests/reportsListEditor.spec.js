describe('Редактор наборов', () => {
  beforeEach(() => {
    return cy.login();
  });

  let linkArray = null;
  let testReportID = null;

  const testMOK = {
    name: 'testing "create-post request"',
    period: '1d',
    factory_number: true,
    ktt: true,
    ktn: true,
    km: true,
    ks: true,
    channels: [
      {
        device_id: 17,
        device_name: 'СЭТ-4ТМ.02M.02',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 13,
        code: 'ra_imp',
        name: 'A+',
        export: false,
        id: '17_ra_imp'
      },
      {
        device_id: 17,
        device_name: 'СЭТ-4ТМ.02M.02',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 13,
        code: 'ra_exp',
        name: 'A-',
        export: true,
        id: '17_ra_exp'
      },
      {
        device_id: 17,
        device_name: 'СЭТ-4ТМ.02M.02',
        device_factory_number: '',
        billing_group_id: 0,
        billing_group_name: '',
        object_id: 2,
        protocol_id: 13,
        code: 'rr_imp',
        name: 'R+',
        export: false,
        id: '17_rr_imp'
      }
    ]
  };

  it('Создание тестового отчета', function() {
    cy.visit('/sedmax/web/ui/unbalance_reports/index')
      .server()
      .route('POST', '/create', [])
      .as('create')
      .wait(1000)
      .getByText(/Добавить отчет/i)
      .click({ force: true })
      .wait(1000)
      .get('#SETTINGS_REPORT_nameDevice', { timeout: 2000 })
      .wait(100)
      .type('Report created(UI) by cypress testing library', { force: true })
      // .get('#SETTINGS_REPORT_factory_number')
      // .click({ force: true })
      // .click({ force: true })
      // .get('#SETTINGS_REPORT_ktt')
      // .click({ force: true })
      // .get('#SETTINGS_REPORT_ktn')
      // .click({ force: true })
      // .click({ force: true })
      // .get('#SETTINGS_REPORT_km')
      // .click({ force: true })
      // .get('#SETTINGS_REPORT_ks')
      // .click({ force: true })
      .wait(200)
      .get('.ant-tree-checkbox-inner')
      .first()
      .click({ multiple: true, force: true })
      .wait(200)
      .get('.ant-table-thead span.ant-checkbox-inner')
      .first()
      .click({ force: true })
      .getByText(/Переместить выбранное/i)
      .click({ force: true, timeout: 0 })

      .getByText(/Сохранить/i)
      .click({ force: true, timeout: 1000 })
      .wait(500)
      .url()
      .should('include', '/view');

    cy.location().then(loc => {
      linkArray = loc.pathname.split('/');
      testReportID = linkArray[linkArray.length - 1];
    });
  });

  it('Удаление тестового отчета', function() {
    cy.visit('/sedmax/web/ui/unbalance_reports/index')
      .wait(1000)
      .get('td')
      .contains('td', 'Report created(UI) by cypress testing library')
      .click('right', { multiple: true, force: true })
      .getByText(/Удалить выбранное/i)
      .click({ force: true })
      .get('.ant-modal-confirm-btns button.ant-btn.ant-btn-primary')
      .click({ force: true })
      .wait(500);
  });

  it('Создание тестового отчета XHR', function() {
    cy.visit('/sedmax/web/ui/unbalance_reports/index');

    cy.request(
      'POST',
      'http://192.168.1.129/sedmax/web/reports/unbalance/create',
      testMOK
    ).then(response => {
      console.log(response);
      expect(response.status).to.eq(200);
      testReportID = response.body.id;

      cy.visit('/sedmax/web/ui/unbalance_reports/index');
    });
  });

  it('Удаление тестового отчета XHR', function() {
    cy.visit('/sedmax/web/ui/unbalance_reports/index');

    cy.request(
      'POST',
      'http://192.168.1.129/sedmax/web/reports/unbalance/remove',
      [parseInt(testReportID)]
    ).then(response => {
      console.log(response);
      expect(response.status).to.eq(200);

      cy.visit('/sedmax/web/ui/unbalance_reports/index');
    });
  });
});
